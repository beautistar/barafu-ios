//
//  Statics.swift
//  driver
//
//  Created by RMS on 11/3/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import Foundation
import ObjectMapper

class Statics: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
      static let amount = "amount"
      static let service = "services"
      static let rating = "rating"
    }

    // MARK: Properties
    public var amount: Float?
    public var service: Int?
    public var rating: String?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){

    }

    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
      amount <- map[SerializationKeys.amount]
      service <- map[SerializationKeys.service]
      rating <- map[SerializationKeys.rating]
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = amount { dictionary[SerializationKeys.amount] = value }
      if let value = service { dictionary[SerializationKeys.service] = value }
      if let value = rating { dictionary[SerializationKeys.rating] = value }
      return dictionary
    }


}
