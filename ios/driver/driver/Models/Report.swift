//
//  Report.swift
//  driver
//
//  Created by RMS on 11/3/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit
import ObjectMapper

class Report: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
      static let amount = "amount"
      static let dateVal = "date"
    }

    // MARK: Properties
    public var amount: Double?
    public var dateVal: String?

    public required init?(map: Map){

    }

    public func mapping(map: Map) {
      amount <- map[SerializationKeys.amount]
      dateVal <- map[SerializationKeys.dateVal]
    }

    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = amount { dictionary[SerializationKeys.amount] = value }
      if let value = dateVal { dictionary[SerializationKeys.dateVal] = value }
      return dictionary
    }
    }
