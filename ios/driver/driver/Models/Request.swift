//
//  Request.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation

class Request:Hashable{
    var hashValue: Int {
        return (travel?.id!)!.hashValue
    }
    
    static func ==(lhs: Request, rhs: Request) -> Bool {
        if lhs.travel?.id == rhs.travel?.id {
            return true
        }else{
            return false
        }
    }
    
    public var travel: Travel?
    public var distance: String?
    public var fromDriver: String?
    public var cost: Double?
    public static var selected : Request?
    init(travel:Travel,distance:Int,fromDriver:Int, cost: Double) {
        self.travel = travel
        self.distance = distanceIntToString(value: distance)
        self.fromDriver = distanceIntToString(value: fromDriver)
        self.cost = cost
    }
    
    init(travel:Travel) {
        self.travel = travel
        self.distance = distanceIntToString(value: travel.distanceBest!)
//        self.fromDriver = distanceIntToString(value: fromDriver)
        self.cost = travel.costBest
    }
    
    
    func distanceIntToString(value:Int)->String{
        if value < 1000 {
            return String(value) + " m"
        }
        else {
            return String(value / 1000) + " km"
        }
    }
}
