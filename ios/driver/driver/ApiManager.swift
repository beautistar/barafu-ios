//
//  ProductApiManager.swift
//  bumerang
//
//  Created by RMS on 2019/9/19.
//  Copyright © 2019 RMS. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiManager: NSObject {
    
    class func getRiderInfoGet(riderId: Int, completion: @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let URL = "http://3.130.44.140/app/api/getRiderInfo/\(riderId)"
        Alamofire.request(URL, method:.get).responseJSON { response in
            switch response.result {
                case .failure: completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let result_code = dict["result_code"].intValue
                    if result_code == 200 {
                        completion(true, dict["rider_info"])
                    } else {
                        completion(false, result_code)
                    }
            }
        }
    }
    
    class func getRiderInfoPost(riderId: Int, completion: @escaping (_ success: Bool, _ response : JSON?) -> ()) {
        
        let request_URL = "http://3.130.44.140/app/api"
        let params = [ "getRiderInfo" : "\(riderId)" ] as [String : Any]
        
        Alamofire.request(request_URL, method:.post, parameters:params).responseJSON { response in
            
            switch response.result {
                
                case .failure: completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let result_code = dict["result_code"].intValue
                    if result_code == 200 { completion(true, dict["rider_info"])
                    } else { completion(false, JSON()) }
            }
        }
        
    }
}
