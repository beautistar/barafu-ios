//
//  DriverSocketManager.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation
import SocketIO
import ObjectMapper
import GoogleMaps
import SwiftyJSON

class DriverSocketManager {
    var socket : SocketIOClient
    var manager: SocketManager
    static let shared = DriverSocketManager()
    var defaults:UserDefaults?
    init() {
        manager = SocketManager(socketURL: URL(string: AppDelegate.info["ServerAddress"] as! String)!)
        socket = manager.socket(forNamespace: "/client")
    }
    
    func connect(token:String,userDefaults:UserDefaults?,completionHandler:@escaping ()->Void) {
        if userDefaults != nil {
            defaults = userDefaults!
        }
        let version = AppDelegate.info["CFBundleVersion"] as! String
        manager = SocketManager(socketURL: URL(string: AppDelegate.info["ServerAddress"] as! String)!,config:[.connectParams(["token" : token, "os" : "ios", "version" : version])])
        socket = manager.socket(forNamespace: "/client")
        socket.on(clientEvent: .connect) {data, ack in
            completionHandler()
        }
        socket.on(clientEvent: .disconnect) {data, ack in
            NotificationCenter.default.post(name: .socketDisconnect, object: data)
        }
        socket.on("error") { data, ack in
            NotificationCenter.default.post(name: .socketError, object: data)
        }
        socket.on("requestReceived") { data, ack in
            let request = Request(travel: Travel(JSON: data[0] as! [String:Any])!)
            NotificationCenter.default.post(name: .requestReceived, object: request)
        }
        socket.on("cancelRequest") { data, ack in
            NotificationCenter.default.post(name: .requestCanceled, object: data[0] as! Int)
        }
        socket.on("riderAccepted") { data, ack in
            Travel.shared = Travel(JSON: data[0] as! [String:Any])!
            Rider.shared = Rider(JSON: data[1] as! [String:Any])!
            NotificationCenter.default.post(name: .riderAccepted, object: nil)
        }
        socket.on("driverInfoChanged") { data, ack in
            AppConfig.shared.user = Driver(JSON: data[0] as! [String:Any])
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
            if self.defaults != nil {
                self.defaults!.set(encodedData, forKey:"settings")
            }
        }
        socket.on("cancelTravel") { data, ack in
            NotificationCenter.default.post(name: .cancelTravel, object: nil)
        }
        socket.connect()
    }
    
    func changeStatus(turnOnline:Bool, completionHandler: @escaping (ServerResponse)->Void){
        socket.emitWithAck("changeStatus", turnOnline ? "online" : "offline").timingOut(after: 15) { data in
            completionHandler(ServerResponse(object: data))
        }
    }
    
    func getStatus(completionHandler:@escaping (ServerResponse,Travel)-> Void) {
        socket.emitWithAck("getStatus").timingOut(after: 15) { data in
            let response = ServerResponse(object: data)
            if response == .OK {
                completionHandler(response,Travel(JSON: data[1] as! [String:Any])!)
            } else {
                completionHandler(response,Travel())
            }
        }
    }
    
    func locationChanged(latitude:Double,longitude:Double){
        socket.emit("locationChanged", latitude,longitude)
    }
    
    func acceptOrder(travelId:Int, completionHandler: @escaping (Travel)-> Void) {
        socket.emitWithAck("driverAccepted", travelId).timingOut(after: 15) { data in
            print("acceptorder response:====", data[0])
            //print("acceptorder response:====", data[0])
            completionHandler(Travel(JSON: data[0] as! [String:Any])!)
        }
    }
    
    func serviceInLocation(){
        socket.emit("buzz")
    }
    
    func callRequest(completionHandler: @escaping (ServerResponse)->Void){
        socket.emitWithAck("callRequest").timingOut(after: 15) { data in
            completionHandler(ServerResponse(object: data))
        }
    }
    
    func startTravel(){
        socket.emit("startTravel")
    }
    
    func cancelService(completionHandler:@escaping (ServerResponse)->Void) {
        socket.emitWithAck("cancelTravel").timingOut(after: 15) { data in
            completionHandler(ServerResponse(object: data))
        }
    }
    
    func notificationPlayerId(playerId: String) {
        socket.emit("notificationPlayerId", playerId)
    }

    func finishService(completionHandler:@escaping (ServerResponse, Bool, Double)->Void) {
        socket.emitWithAck("finishedTaxi", Travel.shared.costBest!,Travel.shared.durationReal!,Travel.shared.distanceReal!,Travel.shared.log ?? "")
            .timingOut(after: 15) { data in
                
//                print("rms_finishService : ", data)
            completionHandler(ServerResponse(object: data),data[1] as! Bool, data[2] as! Double)
                
//                completionHandler(ServerResponse(object: data), true, 300 as Double)
        }
    }
    
    func getTransactions(completionHandler:@escaping (_ transactions:[Transaction])->Void) {
        socket.emitWithAck("getTransactions").timingOut(after: 15) { data in
            let transactions = Mapper<Transaction>().mapArray(JSONArray: data[1] as! [[String : Any]])
            completionHandler(transactions)
        }
    }
    
    func getTravels(completionHandler:@escaping (_ travels:[Travel])->Void) {
        socket.emitWithAck("getTravels").timingOut(after: 15) { data in
            if data.count > 1 {
                let travels = Mapper<Travel>().mapArray(JSONArray: data[1] as! [[String : Any]])
                completionHandler(travels)
            }
        }
    }
    
    func editProfile(profileInfo:String,completionHandler:@escaping (ServerResponse)->Void) {
        socket.emitWithAck("editProfile", profileInfo).timingOut(after: 15) { data in
            completionHandler(ServerResponse(object: data))
        }
    }
    
    func changeProfileImage(imageData:Data,completionHandler:@escaping (ServerResponse,[String:Any]?)->Void) {
        socket.emitWithAck("changeProfileImage", imageData).timingOut(after: 150) { data in
            completionHandler(ServerResponse(object: data), data.count == 2 ? (data[1] as! [String:Any]): nil)
        }
    }
    
    func getRequests(completionHandler:@escaping (ServerResponse,Set<Request>)->Void) {
        socket.emitWithAck("getRequests").timingOut(after: 15) { data in
            let response = ServerResponse(object: data)
            var requestsSet: Set<Request> = Set()
            if response == ServerResponse.OK {
                let requestsArray = data[1] as! [[String:Any]]
                for row in requestsArray {
                    let request = Request(travel: Travel(JSON: row )!)
                    requestsSet.insert(request)
                }
                completionHandler(ServerResponse(object: data),requestsSet)
            } else {
                completionHandler(ServerResponse(object: data),Set<Request>())
            }
        }
    }
    
    func chargeAccount(method:String, token:String, amount: Double, completionHandler: @escaping (ServerResponse,String) -> Void) {
        socket.emitWithAck("chargeAccount", method, token, amount).timingOut(after: 15) { data in
            if data.count == 1 {
                completionHandler(ServerResponse(object: data),"")
            } else {
                completionHandler(ServerResponse(object: data),data[1] as! String)
            }
        }
    }
    
    func sendTravelInfo() {
        self.socket.emit("travelInfo", Travel.shared.distanceReal!, Travel.shared.durationReal!, Travel.shared.costBest!)
    }
    
    func changeHeaderImage(imageData:Data,completionHandler:@escaping (ServerResponse,[String:Any])->Void) {
        socket.emitWithAck("changeHeaderImage", imageData).timingOut(after: 150) { data in
            completionHandler(ServerResponse(object: data),data[1] as! [String:Any])
        }
    }
    
    func requestPayment(completionHandler: @escaping (ServerResponse) -> Void) {
        socket.emitWithAck("requestPayment").timingOut(after: 15) { data in
            completionHandler(ServerResponse(object: data))
        }
    }
    
    //rms
    func requestReject(travelId:Int, completionHandler: @escaping (ServerResponse)-> Void) {
        socket.emitWithAck("requestReject", travelId).timingOut(after: 15) { data in
            completionHandler(ServerResponse(object: data))
        }
    }
    
    func getStatistics(dateType: Int, completionHandler:@escaping (ServerResponse, Statics?, [Report]?)-> Void) {
        socket.emitWithAck("getStats", dateType).timingOut(after: 15) { data in

            print("socket: ", data)
            
            var statics: Statics?
            var report: [Report]?
            
            if let statics1 = data[1] as? [String: Any] {
                statics = Statics(JSON: statics1)
            }
            
            if let report1 = data[2] as? [[String: Any]] {
                report = Mapper<Report>().mapArray(JSONArray: report1)
            }

            completionHandler(ServerResponse(object: data), statics, report)
        }
    }

    
}
