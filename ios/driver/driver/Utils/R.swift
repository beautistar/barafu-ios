//
//  File.swift
//  driver
//
//  Created by RMS on 10/28/19.
//  Copyright © 2019 minimal. All rights reserved.
//
import Foundation

struct R {
    
    struct string {
        
        static let APP_TITLE = "Barafu Driver"
        
        //splash screen
        static let LOADING_MARK = ["Loading...", "Cargando..."]
        static let LOGIN_BUT = ["Login", "Iniciar sesión"]
        
        //main page
        static let PICKUP_1_BUT = ["CONFIRM PICKUP", "CONFIRMAR RECOGIDA"]
        static let PICKUP_2_BUT = ["CONFIRM DESTINATION", "CONFIRMAR DESTINO"]
        
        //menu page
        static let BUT_TRIPHISTORY = ["Trip History", "Historia de viaje"]
        static let BUT_YOURPROFILE = ["Your Profile", "Tu perfil"]
        static let BUT_LOCATION = ["Location", "Localizaciones"]
        static let BUT_WALLET = ["Wallet", "Billetera"]
        static let BUT_COUPONS = ["Coupons", "Cupones"]
        static let BUT_PORMOTIONS = ["Promotions", "Promociones"]
        static let BUT_TRANSACTIONS = ["Transactions", "Actas"]
        static let BUT_SUPPORT = ["Support", "Apoyo"]
        static let BUT_EMERGENCY = ["Emergency", "Emergencia"]
        static let BUT_ABOUT = ["About", "Acerca de"]
        static let BUT_LOGOUT = ["Logout", "Cerrar sesión"]
        
        static let T_BALANCE = ["Balance", "Equilibrar"]
        
        
        
        
        
        
        
        
        
        static let ABOUT_TITLE = ["Be safe, be on time"]
        static let WE_ARE_AN_INNOVATION = ["We are an innovative Tanzanian transportation platform that enables people to gain short-term access to transportation modes on an as-needed basis, at an affordable competitive price."]
        static let THIS_IS_OUR_MOTTO = ["This is our motto at Barafu ride."]
        static let SAFELY_TIME_AND_EXCELLENT = ["Safety, Time and Excellent services is our focus."]
        static let WE_CALL_FOR_EVERY_RIDER = ["We call for every rider and driver to agree to our Community Guiding principles before they can take or give a ride with us. We believe every ride should be a welcoming and safe space for everybody."]
        static let WE_HAVE_A_DEDICATED = ["We have a dedicated Trust and Safety team that’s available to speak with you and assist you through our around-the-clock Critical Response Line."]
        static let CONTACT_US_FOR = ["Contact us for assistance"]
        
        
        //Emergency
        static let EMERGENCY_TITLE = ["Make your travel safe"]
        static let EMERGENCY_TEXT1 = ["Send alert to your friends/family members in case of emergency"]
        static let EMERGENCY_TEXT2 = ["Please add them to your emergency contacts"]
        static let EMERGENCY_ADD = ["ADD CONTACTS"]
        
        //about
        static let ABOUT = ["About", "Acerca de"]
        static let INFO = ["Application Name", "Nombre de la aplicación"]
        
        static let VERSION = ["Version", "Versión"]
        static let WEBSITE = ["Website", "Sitio web"]
        static let PHONENUM = ["Phone Number", "Número de teléfono"]
        static let APPS_ALL = ["Apps All rights reserved","Apps Todos los derechos reservados"]
        
        
        
        static let OK = ["OK", "Okay"]
        static let CANCEL = ["CANCEL", "CANCELAR"]
        static let ERROR = ["Error!", "Error"]
        static let SUCCESS = ["Success!", "Éxito"]
        static let CONNECT_FAIL = ["No internet","No internet"]
        
        

        
        //statistics
        
        static let tabName_1 = ["DAILY", "DIARIO"]
        static let tabName_2 = ["WEEKLY", "SEMANAL"]
        static let tabName_3 = ["MONTHLY", "MENSUAL"]
    }
    
   
    
}
