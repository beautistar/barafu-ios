//
//  StatisticsVC.swift
//  driver
//
//  Created by RMS on 10/31/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit
import SwiftChart
import SwiftyJSON

enum DateType: Int {
    case daily = 0
    case weekly
    case monthly
}

class StatisticsVC: UIViewController, CustomSegmentedControlDelegate {

    @IBOutlet weak var ui_lbltotal: UILabel!
    @IBOutlet weak var ui_lblService: UILabel!
    @IBOutlet weak var ui_lblRating: UILabel!
    
    var dateType: DateType = .daily
    
    @IBOutlet weak var ui_chart: Chart!
    @IBOutlet weak var ui_segument: CustomSegmentedControl!{
        didSet{
            ui_segument.setButtonTitles(buttonTitles: [
                R.string.tabName_1[LANG_KEY],
                R.string.tabName_2[LANG_KEY],
                R.string.tabName_3[LANG_KEY]
            ])
            ui_segument.selectorViewColor = .orange
            ui_segument.selectorTextColor = .orange
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ui_segument.delegate = self
        showChartData(1)
//        self.ui_chart.xLabelsSkipLast = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func changeToIndex(index: Int) {
        showChartData(index + 1)
    }
    
    func setEmptyData(_ statics : Statics?) {
        if statics == nil {
            ui_lbltotal.text = "-"
            ui_lblService.text = "-"
            ui_lblRating.text = "-"
        } else {
            let lblTotal = statics?.amount ?? 0
            ui_lbltotal.text = lblTotal == 0 ? "-" : "\(lblTotal.rounded())"
            
            let lblService = statics?.service ?? 0
            ui_lblService.text = lblService == 0 ? "-" : "\(lblService)"
            
            let rateVal = statics!.rating ?? "-"
            ui_lblRating.text = rateVal != "-" ? String(rateVal.split(separator: ".")[0]) : rateVal
        }
    }
    
    func showChartData(_ index: Int) {
        setEmptyData(nil)
        ui_chart.removeAllSeries()
        
        DriverSocketManager.shared.getStatistics(dateType: index) {(response, statics, report) in
            if response == .OK {
                self.setEmptyData(statics)
                
                if report != nil {
                    var amountVal = [Double]()
                    var dateVal = [String]()
                    var xLabels = [Double]()
                    var key = 0
                    for one in report! {
                        amountVal.append(one.amount!)
                        if index == 1 {
                            dateVal.append(String(one.dateVal!.split(separator: "T")[0]))
                        } else {
                            dateVal.append(String(one.dateVal!))
                        }
                        xLabels.append(Double(key))
                        key += 1
                    }
                    
                    let series = ChartSeries(amountVal)
                    series.color = ChartColors.blueColor()
                    series.area = true
                    self.ui_chart.showYLabelsAndGrid = false
                    xLabels.append(Double(key) - 0.5)
                    dateVal.append("")
                    self.ui_chart.xLabels = xLabels
                    self.ui_chart.xLabelsTextAlignment = .justified
                    self.ui_chart.xLabelsFormatter = {(labelIndex: Int, labelValue: Double) -> String in
                        return dateVal[labelIndex]
                    }
//                    self.ui_chart.xLabelsFormatter = { String(Int(round($1))) + formatString }
                    self.ui_chart.add([series])
                }
            }
        }
    }
}
