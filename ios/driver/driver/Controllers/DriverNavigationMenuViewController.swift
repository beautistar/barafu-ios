//
//  DriverNavigationMenuViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Kingfisher
import KingfisherWebP
import InteractiveSideMenu
import PopupDialog

class DriverNavigationMenuViewController : MenuViewController {
    let kCellReuseIdentifier = "MenuCell"
    let menuItems = ["Main"]
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBalance: UILabel!
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageUser.layer.cornerRadius = imageUser.frame.size.width / 2
        imageUser.clipsToBounds = true
        imageUser.layer.borderColor = UIColor.white.cgColor
        imageUser.layer.borderWidth = 3.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let driverImage = AppConfig.shared.user?.media?.address {
            let processor = DownsamplingImageProcessor(size: imageUser.intrinsicContentSize) >> RoundCornerImageProcessor(cornerRadius: imageUser.intrinsicContentSize.width / 2)
            let url = URL(string: AppDelegate.info["ServerAddress"] as! String + driverImage.replacingOccurrences(of: " ", with: "%20"))
            imageUser.kf.setImage(with: url, placeholder: UIImage(named: "Nobody"), options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5)),
                .cacheOriginalImage
            ]) { result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
        if let user = AppConfig.shared.user {
            labelName.text = "\(user.firstName == nil ? "" : user.firstName!) \(user.lastName == nil ? "" : user.lastName!)"
        }
        
        if let balance = AppConfig.shared.user?.balance {
            
            labelBalance.text = R.string.T_BALANCE[LANG_KEY] + " : " + formattedNumber(Float(balance).rounded()) + currencyUnit[CURRENCY_KEY]
            
            
            
            //labelBalance.text = String(format: "Balance: %.2f $", balance)
//            let captionFormat = NSLocalizedString("navigation.header.balance.value", value: "Balance: %@", comment: "")
//            let amountFormat = NSLocalizedString("base.money", value: "%.0f " + currencyUnit[CURRENCY_KEY], comment: "Default Currency format. 0 stands for digits after '.'")
//            let amount = String(format: amountFormat, balance)
//            labelBalance.text = String(format: captionFormat, amount)
            
        } else {
            labelBalance.text = NSLocalizedString("navigation.header.balance.zero", value: "Balance: 0", comment: "Navigation Header Balance Being Zero")
        }
    }
    
    @IBAction func onWithdrawClicked(_ sender: UIButton) {
        let popup = PopupDialog(title: NSLocalizedString("question.request.payment.title", value: "Request Payment", comment: ""), message: NSLocalizedString("question.request.payment.description", value: "Asking user if is sure to request a payment", comment: "Asking user if is sure to request a payment"))
        
        // Create buttons
        let buttonOne = CancelButton(title: NSLocalizedString("message.button.cancel",value: "Cancel", comment: "Message Cancel Button")) {
        }
        
        let buttonTwo = DefaultButton(title: NSLocalizedString("message.button.yes",value: "Yes", comment: "Message Yes Button")) {
            DriverSocketManager.shared.requestPayment() { res in
                if res == ServerResponse.OK {
                    let popup = DialogBuilder.getDialogForMessage(message: NSLocalizedString("message.payment.requested", value: "Withdraw request was successfully applied. Your payment will be done soon.", comment: ""), completion: nil)
                    self.present(popup, animated: true, completion: nil)
                } else {
                    DialogBuilder.alertOnResponse(response: res)
                }
            }
        }
        popup.addButtons([buttonOne, buttonTwo])
        self.present(popup, animated: true, completion: nil)
        
    }
    
    // Trips History
    @IBAction func onTravelsClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showTravels", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onStatistics(_ sender: Any) {
        
        //showStatistics
        guard let menuContainerViewController = self.menuContainerViewController else {
                   return
               }
               menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showStatistics", sender: nil)
               menuContainerViewController.hideSideMenu()
        
    }
    
    
    // Wallet
    @IBAction func onWalletClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
       
       //showWallet
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showWalletCard", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    // Transactions
    @IBAction func onTransactionsClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showTransactions", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    // Your Profile
    @IBAction func onProfileClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showEditProfile", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    // Support
    @IBAction func onSupportClick(_ sender: UIButton) {
        
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showSupport", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    // Emergency
    @IBAction func onEmergencyClick(_ sender: UIButton) {
        
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showEmergency", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    // About
    @IBAction func onAboutClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }

        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showAbout", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    // Exit
    @IBAction func onExitClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        if let bundle = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundle)
            //self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            menuContainerViewController.hideSideMenu()
        }
    }
}
