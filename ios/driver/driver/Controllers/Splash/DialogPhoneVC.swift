//
//  DialogPhoneVC.swift
//  driver
//
//  Created by RMS on 11/12/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit
import SKCountryPicker

class DialogPhoneVC: UIViewController {

    var parentVC : SplashViewController? = nil
    var countryCode = "+1"
    
    @IBOutlet weak var ui_imgFlag: UIImageView!
    @IBOutlet weak var ui_butCountry: UIButton!
    @IBOutlet weak var ui_txtPhoneNum: UITextField!

    @IBOutlet weak var ui_viewPWD: UIView!
    @IBOutlet weak var ui_txtPWD: UITextField!

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ui_viewPWD.isHidden = true
        
        guard let country = CountryManager.shared.currentCountry else {
            self.ui_butCountry.setTitle("Pick Country", for: .normal)
            self.ui_imgFlag.isHidden = true
            return
        }

        
        ui_butCountry.setTitle(country.dialingCode, for: .normal)
        countryCode = country.dialingCode!
        ui_imgFlag.image = country.flag
        ui_butCountry.clipsToBounds = true
    }
    
    @IBAction func onCountryCodeTaped(_ sender: Any) {

        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

            guard let self = self else { return }

            self.countryCode = country.dialingCode!
            print("country.countryName: ", country.countryName)
            print("country.countryCode: ", country.countryCode)
            print("country.digitCountrycode: ", country.digitCountrycode)
            print("country.dialingCode: ", country.dialingCode)
            
            
            self.ui_imgFlag.image = country.flag
            self.ui_butCountry.setTitle(country.dialingCode, for: .normal)

        }

        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
        CountryManager.shared.addFilter(.countryCode)
        CountryManager.shared.addFilter(.countryDialCode)
        
    }
    
    @IBAction func onBackTaped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onOKTaped(_ sender: Any) {
        
        if ui_txtPhoneNum.text!.count == 0 || countryCode == "" {
            return
        }
        
        if countryCode == "+255" && ui_txtPhoneNum.text == "718238477" {
            
            if ui_viewPWD.isHidden {
                ui_viewPWD.isHidden = false
                
            } else if ui_txtPWD.text != "123456" {
                let message = "Please input password correctly!"
                
                let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler :nil))
                
                DispatchQueue.main.async(execute:  {
                    self.present(alert, animated: true, completion: nil)
                })
            } else {
                gotoNextVC()
            }
        } else {
            gotoNextVC()
        }
    }
    
    func gotoNextVC() {
        if let parentVC = parentVC {
            parentVC.countryCode = countryCode
            parentVC.phoneNumSubView = ui_txtPhoneNum.text!
            parentVC.closeSubVC()
        }
    }
}
