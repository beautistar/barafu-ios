//
//  WalletCardVC.swift
//  driver
//
//  Created by RMS on 11/1/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit

class WalletCardVC: UIViewController {

    @IBOutlet weak var ui_balance: UILabel!
    @IBOutlet weak var ui_lblCurrentBalance: UILabel!
    
    @IBOutlet weak var ui_lblSelectPay: UILabel!
    @IBOutlet weak var ui_lblAmount: UILabel!
    
    @IBOutlet weak var ui_but1: UIButton!
    @IBOutlet weak var ui_but2: UIButton!
    @IBOutlet weak var ui_but3: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let balance = AppConfig.shared.user?.balance {
        
            ui_balance.text = currencyUnit[CURRENCY_KEY] + " " + formattedNumber(Float(balance).rounded())
        }
    }

    @IBAction func onTapedMeda(_ sender: Any) {
        guard let url = URL(string: "itms://apps.apple.com/us/app/tigo-pesa-tanzania/id825924072") else { return }
        if UIApplication.shared.canOpenURL(url) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func onTapedTigo(_ sender: Any) {
        guard let url = URL(string: "itms://apps.apple.com/us/app/m-pesa-tanzania/id1313948420") else { return }
        if UIApplication.shared.canOpenURL(url) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func onTapedAirtel(_ sender: Any) {
        guard let url = URL(string: "itms://apps.apple.com/us/app/my-airtel-africa/id1462268018") else { return }
               if UIApplication.shared.canOpenURL(url) {
                   
                   if #available(iOS 10.0, *) {
                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
    }
    
    @IBAction func onTapedCheckout(_ sender: Any) {
    }
    
}
