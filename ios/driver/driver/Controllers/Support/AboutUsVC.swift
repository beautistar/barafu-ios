//
//  AboutUsVC.swift
//  driver
//
//  Created by RMS on 10/28/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var ui_lblTitleTop: UILabel!
    @IBOutlet weak var ui_lblContent1: UILabel!
    @IBOutlet weak var ui_lblContent2: UILabel!
    @IBOutlet weak var ui_lblContent3: UILabel!
    @IBOutlet weak var ui_lblContent4: UILabel!
    @IBOutlet weak var ui_lblContent5: UILabel!
    @IBOutlet weak var ui_lblAddrTitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTextData()
    }
    
    func setTextData() {
        ui_lblTitleTop.text = R.string.ABOUT_TITLE[0]
        ui_lblContent1.text = R.string.THIS_IS_OUR_MOTTO[0]
        ui_lblContent2.text = R.string.WE_ARE_AN_INNOVATION[0]
        ui_lblContent3.text = R.string.SAFELY_TIME_AND_EXCELLENT[0]
        ui_lblContent4.text = R.string.WE_CALL_FOR_EVERY_RIDER[0]
        ui_lblContent5.text = R.string.WE_HAVE_A_DEDICATED[0]
        
        ui_lblAddrTitle.text = R.string.CONTACT_US_FOR[0]
    }
}

