//
//  SupportCell.swift
//  driver
//
//  Created by RMS on 10/28/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit

class SupportCell: UITableViewCell {
    
    @IBOutlet weak var ui_imgIcon_D: UIImageView!
    @IBOutlet weak var ui_lblIcon_D: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(_ imgName:String, _ title:String, _ state: Bool) {
        ui_imgIcon_D.image = UIImage(named: imgName)
        ui_lblIcon_D.text = title
        
        if state {
//            self.accessoryType = .disclosureIndicator
//            self.accessoryType = .detailDisclosureButton
//            self.selectionStyle = .blue 
        }
    }

}
