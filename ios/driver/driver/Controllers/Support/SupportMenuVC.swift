//
//  SupportMenuVC.swift
//  driver
//
//  Created by RMS on 10/28/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit

class SupportMenuVC: UIViewController {
    
    let imageNames = ["ic_aboutTest", "ic_privacy", "ic_terms", "ic_contactus", "ic_help"]
    let titles = ["About Us Test", "Privacy Policy", "Terms and Condoition", "Contact Us", "Help"]
    
    @IBOutlet weak var tblSupport: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblSupport.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
        
    }
    var toVCArr = ["AboutUsVC", "AboutUsVC", "AboutUsVC", "AboutUsVC", "AboutUsVC"]
    fileprivate func gotoNext(_ index: Int) {
        
        if index > 0 {
            return
        }
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: toVCArr[index])
        
        self.navigationController?.pushViewController(toVC!, animated: true)
    }
}

extension SupportMenuVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
           return 5
       }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SupportCell", for: indexPath) as! SupportCell
        var state = false
        if indexPath.row == 0 {
            state = true
        }
        cell.setCell(imageNames[indexPath.row], titles[indexPath.row], state)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        
        gotoNext(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}
