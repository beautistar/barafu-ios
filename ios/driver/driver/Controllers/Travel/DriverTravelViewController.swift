//
//  DriverTravelViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import PopupDialog
import MTSlideToOpen
import RMPickerViewController


import Kingfisher
import KingfisherWebP
import SwiftyJSON

class DriverTravelViewController: UIViewController, CLLocationManagerDelegate, MTSlideToOpenDelegate {
    enum BarButtonMode {
        case notificate,call
    }
    var barButtonMode = BarButtonMode.notificate
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var map:GMSMapView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    
    var userLocation = CLLocation()
    var timer = Timer()
    var isBuzzed = false
    var isTravelStarted = false
    var timeGone: Int = 0
    var timeWait: Int = 0
    var locationManager = CLLocationManager()
    var driverMarker = GMSMarker()
    var riderMarker = GMSMarker()
    var polyline = GMSPolyline()
    var route = [CLLocationCoordinate2D]()
    @IBOutlet weak var slideStart: MTSlideToOpenView!
    @IBOutlet weak var slideCancel: MTSlideToOpenView!
    @IBOutlet weak var slideFinish: MTSlideToOpenView!
    
    var riderInfo = JSON()
//    let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
//    let dic = NSDictionary(contentsOfFile: path) as! [String: Any]
//    let server_url = dic["ServerAddress"] as! String
    let server_url = "http://3.130.44.140:8080/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slideStart.delegate = self
        slideFinish.delegate = self
        slideCancel.delegate = self
        slideStart.defaultLabelText = NSLocalizedString("travel.slide.start", value: "Slide To Start", comment: "Travel Slider Start")
        slideFinish.defaultLabelText = NSLocalizedString("travel.slide.finish", value: "Slide to finish", comment: "Travel Slider Finish")
        slideCancel.defaultLabelText = NSLocalizedString("travel.slide.cancel", value: "Slide To Cancel", comment: "Travel Slider Cancel")
        slideStart.thumnailImageView.image = #imageLiteral(resourceName: "start")
        slideStart.sliderViewTopDistance = 6
        slideStart.sliderCornerRadius = 15
        
        slideFinish.thumnailImageView.image = #imageLiteral(resourceName: "finish")
        slideFinish.sliderViewTopDistance = 6
        slideFinish.sliderCornerRadius = 15
        
        slideCancel.thumnailImageView.image = #imageLiteral(resourceName: "cross")
        slideCancel.sliderViewTopDistance = 6
        slideCancel.sliderCornerRadius = 15
        NotificationCenter.default.addObserver(self, selector: #selector(self.travelCanceled), name: .cancelTravel, object: nil)
        if let cost = Travel.shared.costBest {
            
            labelCost.text = formattedNumber(Float(cost).rounded()) + currencyUnit[CURRENCY_KEY]
            
//            labelCost.text = String(format: NSLocalizedString("base.money", value: "%.0f " + currencyUnit[CURRENCY_KEY], comment: "Default Currency format. 0 stands for digits after '.'"), cost)
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 50
        locationManager.activityType = .automotiveNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
//        driverMarker.icon = #imageLiteral(resourceName: "marker taxi")
        driverMarker.icon = UIGraphicsImageRenderer(size: .init(width: 26.0, height: 57.0)).image { context in
            UIImage(named: "marker taxi")!.draw(in: .init(origin: .zero, size: context.format.bounds.size))
        }
        riderMarker.icon = #imageLiteral(resourceName: "marker pickup")
        riderMarker.position = (Travel.shared.pickupPoint)!
        riderMarker.map = map
        self.navigationItem.hidesBackButton = true
        
        let startPoint = "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)"
        let endPoint = "\(Travel.shared.pickupPoint!.latitude ),\(Travel.shared.pickupPoint!.longitude)"
        drawMapPath(origin: startPoint, destination: endPoint)
        
        if Travel.shared.startTimestamp != nil {
            startTravelUI()
        }
        
        getRiderInfo()
    }
    
    func setRiderInfo(_ riderInfo : JSON) {
        
        print("rms_riderInfor", riderInfo)
        
        if riderInfo["media_url"].stringValue != "" {
            let image = riderInfo["media_url"].stringValue
            let driverImage = server_url + image.replacingOccurrences(of: " ", with: "%20")
            let url = URL(string: driverImage)
            
            let processor = DownsamplingImageProcessor(size: imageAvatar.intrinsicContentSize) >> RoundCornerImageProcessor(cornerRadius: imageAvatar.intrinsicContentSize.width / 2)
            
            
            imageAvatar.kf.setImage(with: url, placeholder: UIImage(named: "Nobody"), options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5)),
                .cacheOriginalImage
            ]) { result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
        
        //labelUsername.text = Travel.shared.rider!.firstName! + " " + Travel.shared.rider!.lastName!
        labelUsername.text = riderInfo["first_name"].stringValue + " " + riderInfo["last_name"].stringValue
    }
    
    @IBAction func onRightBarButtonClicked(_ sender: UIBarButtonItem) {
        if barButtonMode == .notificate{
            DriverSocketManager.shared.serviceInLocation()
            barButtonMode = .call
            sender.image = #imageLiteral(resourceName: "call")
        } else {
            showContactModeChoose()
        }
    }
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        sender.isEnabled = false
        sender.resetStateWithAnimation(true)
        switch sender {
        case slideCancel:
            self.cancelTravel()
            break
        case slideStart:
            self.startTravel()
            break
        case slideFinish:
            if route.count > 2 {
                timer.invalidate()
                LoadingOverlay.shared.showOverlay(view: self.view)
                Travel.shared.log = MapsUtil.encode(items: MapsUtil.simplify(items: route, tolerance: 50))
                DriverSocketManager.shared.finishService() { (response, usedCredit, cost) in
                    LoadingOverlay.shared.hideOverlayView()
                    _ = self.navigationController?.popViewController(animated: true)
                }
            } else {
                timer.invalidate()
                LoadingOverlay.shared.showOverlay(view: self.view)
                DriverSocketManager.shared.finishService() { (response, usedCredit, cost) in
                    LoadingOverlay.shared.hideOverlayView()
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            
            break
        default:
            break;
        }
    }
    
    func startTravel() {
        startTravelUI()
        startTravelNetwork()
        
        let startPoint = "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)"
//        let startPoint = "\(Travel.shared.pickupPoint!.latitude),\(Travel.shared.pickupPoint!.longitude)"
        
        let endPoint = "\(Travel.shared.destinationPoint!.latitude ),\(Travel.shared.destinationPoint!.longitude)"
        
        drawMapPath(origin: startPoint, destination: endPoint)
    }
    
    func drawMapPath(origin: String, destination: String) {
        
        //delete draw
        polyline.map = nil
        
        //Setting the start and end location
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
        let dic = NSDictionary(contentsOfFile: path) as! [String: Any]
         GMSPlacesClient.provideAPIKey(dic["GoogleMapsAPIKey"] as! String)
        
        let urls = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(dic["GoogleMapsAPIKey"] as! String)"
        
        let url = URL(string: urls)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in

            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    print(json)
                    let routes = json["routes"] as! NSArray
                    OperationQueue.main.addOperation({
                        for route in routes {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeColor = .black
                            polyline.strokeWidth = 3

                            let bounds = GMSCoordinateBounds(path: path!)
                            self.map!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))

                            polyline.map = self.map
                        }
                    })
                }catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()
    }
    
    func startTravelUI() {
        
        riderMarker.icon = #imageLiteral(resourceName: "marker destination")
        riderMarker.position = (Travel.shared.destinationPoint)!
//        riderMarker.map = map
//        let bounds = GMSCoordinateBounds(coordinate: riderMarker.position, coordinate: driverMarker.position)
//        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
//        map.animate(with: update)

        map.animate(to: GMSCameraPosition.camera(withTarget: userLocation.coordinate, zoom: 18))
        slideCancel.isHidden = true
        slideStart.isHidden = true
        slideFinish.isHidden = false
        isTravelStarted = true
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.onEachSecond), userInfo: nil, repeats: true)
    }
    
    func startTravelNetwork() {
        DriverSocketManager.shared.startTravel()
    }
    
    func getRiderInfo() {
        
        ApiManager.getRiderInfoGet(riderId: Travel.shared.rider_id!) { (isSuccess, data) in
                    
            if isSuccess {
                print("rider_info", data as Any)
                self.riderInfo = JSON(data!)
                self.setRiderInfo(self.riderInfo)
            } else {
                if data == nil {
                    //self.showToast(R.string.CONNECT_FAIL[LANG_KEY])
                } else {
                    //self.showToast(JSON(data!).stringValue)
                }
            }
        }
    }
    
    func cancelTravel() {
        
        polyline.map = nil
        
        let title = NSLocalizedString("question.travel.cancel.title", value: "Cancel Travel", comment: "Message title for asking user on being sure to cancel travel")
        let message = NSLocalizedString("question.travel.cancel.description", value: "Are you sure you want to cancel travel?", comment: "Message description for asking user on being sure to cancel travel")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: NSLocalizedString("message.button.cancel",value: "Cancel", comment: "Message Cancel Button")) {
            self.slideCancel.isEnabled = true
            popup.dismiss()
        }
        
        let buttonTwo = DefaultButton(title: NSLocalizedString("message.button.yes",value: "Yes", comment: "Message Yes Button")) {
            DriverSocketManager.shared.cancelService() { data in
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func travelCanceled() {
        polyline.map = nil
        let title = NSLocalizedString("message.travel.cancel.title", value: "Canceled!", comment: "Message title for leting know their travel was canceled. Either by him/herself or other party.")
        let message = NSLocalizedString("message.travel.cancel.description", value: "The travel was canceled.", comment: "Message description for leting know their travel was canceled. Either by him/herself or other party.")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        
        let buttonOne = DefaultButton(title: NSLocalizedString("message.button.ok",value: "OK", comment: "Message OK button")) {
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func onEachSecond() {
        timeGone = timeGone + 1
        Travel.shared.cost = calculateCost()
        Travel.shared.durationReal = timeWait + timeGone
        labelTime.text = computeTime(time: timeGone + timeWait)
        labelDistance.text = String(format:NSLocalizedString("base.distance.km", value: "%.1f km", comment: "Default format for distances in km"), (Double(Travel.shared.distanceReal!) / 1000.0))
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations[0] as CLLocation
        if route.count > 0 {
            let distance = Int(userLocation.distance(from: CLLocation(latitude: route[route.count - 1].latitude, longitude: route[route.count - 1].longitude)))
            Travel.shared.distanceReal! += distance
        }
        DriverSocketManager.shared.locationChanged(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
  
        route.append(userLocation.coordinate)
          
        driverMarker.position = userLocation.coordinate
        if driverMarker.map == nil {
            driverMarker.map = map
        }
        
        if !isTravelStarted {
            let startPoint = "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)"
            let endPoint = "\(Travel.shared.destinationPoint!.latitude ),\(Travel.shared.destinationPoint!.longitude)"
            drawMapPath(origin: startPoint, destination: endPoint)
            
            let bounds = GMSCoordinateBounds(coordinate: riderMarker.position, coordinate: driverMarker.position)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
            map.animate(with: update)
            
        } else {
            
            map.animate(to: GMSCameraPosition.camera(withTarget: userLocation.coordinate, zoom: 18))
            
        }

        DriverSocketManager.shared.sendTravelInfo()
    }
    
    func computeTime(time:Int)->String{
        if time == 0 {
            return "00:00"
        }
        let sec = time % 60
        let min = time / 60
        return String(format: "%02d':%02d\"", arguments: [min,sec])
    }
    
    func calculateCost() -> Double {
        return Travel.shared.costBest!
        /*let costDistance = (Double(Travel.shared.travelDistance) / 1000) * (TravelType.current?.everyKm)!
         let costTimeGone = (Double(timeGone) / 60) * (TravelType.current?.everyMinuteGone)!
         let costTimeWait = (Double(timeWait) / 60) * (TravelType.current?.everyMinuteWait)!
         let cost = (TravelType.current?.initial)! + costDistance + costTimeGone + costTimeWait
         
         if cost > 0 {
         return 0
         } else {
         return cost
         }*/
    }
    
    func showContactModeChoose(){
        let style = RMActionControllerStyle.white
        let selectAction = RMAction<UIPickerView>(title: NSLocalizedString("message.button.select",value: "Select", comment: "Message Select Button"), style: .done) { controller in
            if controller.contentView.selectedRow(inComponent: 0) == 0 {
                self.directCall()
            } else {
                self.requestCall()
            }
        }
        
        let cancelAction = RMAction<UIPickerView>(title: NSLocalizedString("message.button.cancel",value: "Cancel", comment: "Message Cancel Button"), style: .cancel) { _ in
        }
        
        let actionController = RMPickerViewController(style: style, title: NSLocalizedString("travel.picker.contact.title", value: "Contact Method", comment: "Contact Method picker title"), message: NSLocalizedString("travel.picker.contact.description", value: "Select a contact method", comment: "Contact method picker description"), select: selectAction, andCancel: cancelAction)!;
        
        actionController.picker.delegate = self;
        actionController.picker.dataSource = self;
        
        if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            actionController.modalPresentationStyle = UIModalPresentationStyle.popover
        }
        self.present(actionController, animated: true, completion: nil)
    }
    
    func directCall() {
        if riderInfo["mobile_number"].intValue != 0 {
            if let url = URL(string: "tel://\(riderInfo["mobile_number"].intValue)"),
            UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    // Fallback on earlier versions
                    let dialog = DialogBuilder.getDialogForMessage(message: NSLocalizedString("message.call.request.sent", value: "You can not call as  you haven't any phone number.", comment: ""), completion: nil)
                    self.present(dialog, animated: true, completion: nil)
                }
            }
        }
    }
    
    func requestCall() {
        DriverSocketManager.shared.callRequest() { data in
            if data.rawValue != 200 {
                DialogBuilder.alertOnResponse(response: data)
                return
            }
            let dialog = DialogBuilder.getDialogForMessage(message: NSLocalizedString("message.call.request.sent", value: "Call request has been sent. A call to rider will be made soon.", comment: ""), completion: nil)
            self.present(dialog, animated: true, completion: nil)
        }
    }
}
extension DriverTravelViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    // MARK: UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return NSLocalizedString("travel.contact.direct", value: "Direct Call", comment: "Contact Method Direct. Used in picker.")
        } else {
            return NSLocalizedString("travel.contact.request", value: "Request Call", comment: "Contact Method Request. Used in picker.")
        }
    }
}
