//
//  DriverMainViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import GoogleMaps
import PopupDialog
import iCarousel

class DriverMainViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var buttonStatus: UIBarButtonItem!
    @IBOutlet weak var requestsList: iCarousel!
    @IBOutlet weak var map: GMSMapView!
    
    var requests : Set<Request> = []
    var markerTaxi = GMSMarker()
    var markerPickup = GMSMarker()
    var markerDropOff = GMSMarker()
    var locationManager = CLLocationManager()
    var isOnline = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRequestReceived), name: .requestReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRequestCanceled), name: .requestCanceled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRiderAccepted), name: .riderAccepted, object: nil)
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 50
        locationManager.activityType = .automotiveNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
//        map.
        
        requestsList.dataSource = self
        requestsList.delegate = self
        requestsList.type = .linear
        DriverSocketManager.shared.getStatus() { response,travel in
            if response == .NOT_FOUND {
                return
            }
            let title = NSLocalizedString("message.title.default",value: "Message", comment: "Message Default Title")
            let message = NSLocalizedString("message.content.unfinished.travel", value: "There is an unfinished travel found. After tapping OK you will be redirected to Travel screen.", comment: "")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message)
            // Create buttons
            let buttonOne = DefaultButton(title: NSLocalizedString("message.button.ok",value: "OK", comment: "Message OK button")) {
                Travel.shared = travel
                self.performSegue(withIdentifier: "startTravel", sender: nil)
            }
            
            popup.addButtons([buttonOne])
            self.present(popup, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.refreshRequests()
    }
    
    func refreshRequests() {
        DriverSocketManager.shared.getRequests() { response, requestsSet in
            if response != ServerResponse.OK {
                self.requestsList.isHidden = true
                self.buttonStatus.image = UIImage(named: "topbar_online")
                self.isOnline = false
                return
            }
            self.buttonStatus.image = UIImage(named: "topbar_offline")
            self.isOnline = true
            if(requestsSet.count > 0) {
                self.requestsList.isHidden = false
            }
            self.requests = requestsSet
            self.requestsList.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @IBAction func onDriverStatusClicked(_ sender: UIBarButtonItem) {
        buttonStatus.isEnabled = false
        DriverSocketManager.shared.changeStatus(turnOnline: isOnline) { res in
            self.buttonStatus.isEnabled = true
            if res != .OK  {
                DialogBuilder.alertOnResponse(response: res)
                return
            }
            self.isOnline = !self.isOnline
            if self.isOnline {
                self.buttonStatus.image = UIImage(named: "topbar_online")
                DriverSocketManager.shared.locationChanged(latitude: self.markerTaxi.position.latitude, longitude: self.markerTaxi.position.longitude)
            } else {
                self.buttonStatus.image = UIImage(named: "topbar_offline")
            }
            
        }
    }
    
    @IBAction func onMenuClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .menuClicked, object: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        if isOnline {
            DriverSocketManager.shared.locationChanged(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        }
        markerTaxi.position = userLocation.coordinate
        markerTaxi.appearAnimation = .none
        markerTaxi.rotation = manager.location?.course ?? 0
        markerTaxi.icon = UIGraphicsImageRenderer(size: .init(width: 26.0, height: 57.0)).image { context in
            UIImage(named: "marker taxi")!.draw(in: .init(origin: .zero, size: context.format.bounds.size))
        }
        markerTaxi.map = map
        map.animate(to: GMSCameraPosition.camera(withTarget: userLocation.coordinate, zoom: 17))
    }
    
    @IBAction func onRefreshRequestsClicked(_ sender: UIBarButtonItem) {
        self.refreshRequests()
    }
    
    @objc func onRequestReceived(_ notification: Notification) {
        if let request = notification.object as? Request {
            requestsList.isHidden = false
            requests.insert(request)
            requestsList.reloadData()
        }
    }
    
    @objc func onRequestCanceled(_ notification: Notification) {
        if let travelId = notification.object as? Int {
            for req in requests {
                if(req.travel?.id == travelId) {
                    requests.remove(req)
                    requestsList.reloadData()
                    break;
                }
            }
        }
    }
    
    @objc func onRiderAccepted(_ notification: Notification) {
        LoadingOverlay.shared.hideOverlayView()
        self.performSegue(withIdentifier: "startTravel", sender: nil)
    }
}

extension DriverMainViewController: iCarouselDataSource, iCarouselDelegate{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return requests.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let vc = Bundle.main.loadNibNamed("RequestCard", owner: self, options: nil)?[0] as! RequestCard
        vc.layer.cornerRadius = 10
        vc.layer.shadowOpacity = 0.2
        vc.layer.shadowOffset = CGSize(width: 0, height: 0)
        vc.layer.shadowRadius = 4.0
        let shadowRect: CGRect = vc.bounds;
        vc.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
        let request = requests[requests.index(requests.startIndex, offsetBy: index)]
        /*vc.labelOrigin.text = request.travel!.pickupAddress
        vc.labelDestination.text = request.travel!.destinationAddress*/
        vc.request = request
                
        //var distanceArr = request.distance?.components(separatedBy: " ")
        /*vc.labelDistance.text = distanceArr?[0]
        vc.labelDistanceUnit.text = distanceArr?[1]*/
//        let fromDriverArr = request.fromDriver?.components(separatedBy: " ")
        //vc.labelFromYou.text = fromDriverArr?[0]
        //vc.labelFromYouUnit.text = fromDriverArr?[1]
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
            vc.delegate?.reject(request: request)// Your code with delay
        }
        
        vc.setContentData(request)
        vc.delegate = self
        return vc
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
}
extension DriverMainViewController: DriverRequestCardDelegate {
    func accept(request: Request) {
        LoadingOverlay.shared.showOverlay(view: self.navigationController?.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
            LoadingOverlay.shared.hideOverlayView()
        }
        DriverSocketManager.shared.acceptOrder(travelId: request.travel!.id!) { travel in
            
            Travel.shared = travel
            LoadingOverlay.shared.hideOverlayView()
            self.performSegue(withIdentifier: "startTravel", sender: nil)
        }
    }
    
    func reject(request: Request) {
        requests.remove(request)
//        requests.remove(at: requestsList.currentItemIndex)
        
//            for req in requests {
//                if(req.travel?.id == travelId) {
//                    requests.remove(req)
//                    requestsList.reloadData()
//                    break;
//                }
//        }
        
        requestsList.reloadData()
        
        /*DriverSocketManager.shared.requestReject(travelId: request.travel!.id!) { res in
            if res == ServerResponse.OK {
                self.requests.remove(request)
    //            self.requests.remove(at: requestsList.currentItemIndex)
                self.requestsList.reloadData()
            } else {
                DialogBuilder.alertOnResponse(response: res)
            }
        }
*/
    }
}



