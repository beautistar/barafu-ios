//
//  RequestCard.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit

class RequestCard: UIView {
    @IBOutlet weak var labelPickupLocation: UILabel!
    @IBOutlet weak var labelDestinationLocation: UILabel!
    @IBOutlet weak var labelFromYou: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var buttonAccept: UIButton!
    @IBOutlet weak var buttonReject: UIButton!
    @IBOutlet weak var labelCost: UILabel!
    
    var request: Request?
    var delegate: DriverRequestCardDelegate?
    @IBAction func onAcceptTouched(_ sender: UIButton) {
        delegate?.accept(request: request!)
    }
    
    @IBAction func onRejectTouched(_ sender: Any) {
        delegate?.reject(request: request!)
    }
//    
//    public required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        //setupView()
//    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
//    public override init(frame: CGRect) {
//        super.init(frame: frame)
//        //setupView()
//        print("rmsoverride ; ", self.request!)
//
////        setContentData(self.request!)
//
//    }ddddddddd
//
//    public convenience init(image: UIImage, request: Request) {
//        self.init(frame: .zero)
//        self.labelPickupLocation.text = request.travel?.pickupAddress
//        self.labelDestinationLocation.text = request.travel?.destinationAddress
//        self.labelFromYou.text = request.fromDriver
//        self.labelDistance.text = String(format: NSLocalizedString("base.distance.km", value: "%.1f km", comment: "Default format for distances in km"), Double((request.travel?.distanceBest)!) / 1000.0)
//        self.labelCost.text = String(format: NSLocalizedString("base.money", value: "%.0f " + currencyUnit[CURRENCY_KEY], comment: "Default Currency format. 0 stands for digits after '.'"), Double((request.travel?.costBest)!))
//    }
    
    func setContentData(_ request: Request) {
        
        self.labelPickupLocation.text = request.travel?.pickupAddress
        self.labelDestinationLocation.text = request.travel?.destinationAddress
        self.labelFromYou.text = request.fromDriver
        self.labelDistance.text = String(format: NSLocalizedString("base.distance.km", value: "%.1f km", comment: "Default format for distances in km"), Double((request.travel?.distanceBest)!) / 1000.0)
        
        
        
        self.labelCost.text = formattedNumber(Float(request.travel!.costBest!).rounded()) + currencyUnit[CURRENCY_KEY]
        
        
//        self.labelCost.text = String(format: NSLocalizedString("base.money", value: "%.0f " + currencyUnit[CURRENCY_KEY], comment: "Default Currency format. 0 stands for digits after '.'"), Double((request.travel?.costBest)!))
    }
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        
        // Create, add and layout the children views ..
    }
}
protocol DriverRequestCardDelegate: AnyObject {
    func accept(request:Request)
    func reject(request:Request)
}
