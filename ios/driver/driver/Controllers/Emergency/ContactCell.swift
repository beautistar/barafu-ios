//
//  ContactCell.swift
//  rider
//
//  Created by RMS on 11/1/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import Foundation
import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var ui_avatar: UIImageView!
    @IBOutlet weak var ui_username: UILabel!
    @IBOutlet weak var ui_phone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //, _ imgName: String
    func setCell(_ username: String, _ phone:String) {
//        ui_imgIcon.image = UIImage(named: imgName)
        ui_username.text = username
        ui_phone.text = phone
        
    }
}
