//
//  MyContactsVC.swift
//  rider
//
//  Created by RMS on 11/1/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit
import ContactsUI
import PopupDialog
import StatusAlert


class MyContactsVC: UIViewController {
    
    @IBOutlet weak var ui_tblContactDriver: UITableView!
    
    var contactStore = CNContactStore()
    var contactModels = [ContactModel]()
    var selectedContacts = [ContactModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ui_tblContactDriver.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: #selector(rightbuttonPressed))
        
        selectedContacts = savedContact
        getSavedContacts()
        fetchContact()
    }
    
    @objc func rightbuttonPressed() {
        savedContact = selectedContacts
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: savedContact)
        UserDefaults.standard.set(encodedData, forKey: "emg_contacts")
        UserDefaults.standard.synchronize()
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchContact() {
      
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactThumbnailImageDataKey] as [Any]

        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }

        var results: [CNContact] = []

        for container in allContainers {
            
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
                
            } catch {
                print("Error fetching containers")
            }
        }
        
        for contact in results {
            let username = contact.givenName + " " + contact.familyName
//            var numbers = [String]()
            for one in contact.phoneNumbers {
//                numbers.append(one.value.stringValue)
                let contactModel = ContactModel(name: username, number: one.value.stringValue)   //, avatar: contact.thumbnailImageData
                contactModels.append(contactModel)
            }
            
        }
        ui_tblContactDriver.reloadData()
    }
    
    func setContact(_ index: Int) {
        
        var savedFlag = false
        var selectedkey = 0
        for key in 0 ..< selectedContacts.count {
            if selectedContacts[key].name! == contactModels[index].name && selectedContacts[key].number! == contactModels[index].number {
                savedFlag = true
                selectedkey = key
                break
            }
        }
        
        if savedFlag {
//            let title = "Noties"
            let message = "The contact is saved already. do you delete this contact?"
            
            let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "DELETE", style: .default, handler : {(action) -> Void in
                self.selectedContacts.remove(at: selectedkey)
            }))
            
            DispatchQueue.main.async(execute:  {
                self.present(alert, animated: true, completion: nil)
            })
        } else {
            selectedContacts.append(contactModels[index])
        }
    }
}

extension MyContactsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
        
        cell.setCell(contactModels[indexPath.row].name!, contactModels[indexPath.row].number!)   //, contactModels[indexPath.row].avatar
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.setContact(indexPath.row)
    }
    
}
