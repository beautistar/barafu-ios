//
//  EmergencyViewController.swift
//  driver
//
//  Created by RMS on 10/29/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit
import ContactsUI

var savedContact = [ContactModel]()

func getSavedContacts() {

    if let data = UserDefaults.standard.data(forKey: "emg_contacts"),
        let contactdata = NSKeyedUnarchiver.unarchiveObject(with: data) as? [ContactModel] {
        savedContact = contactdata
        print("rmsrmsrmsrms: ", savedContact.count)

    } else {
        print("error")
    }
}


class EmergencyViewController: UIViewController {

    @IBOutlet weak var EmergencyTitle: UILabel!

    @IBOutlet weak var ui_butAdd: UIButton!
    @IBOutlet weak var ui_lblText_1D: UILabel!
    @IBOutlet weak var ui_lblText_2D: UILabel!
    
    @IBOutlet weak var ui_tblContact: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ui_tblContact.tableFooterView = UIView()
        setTextData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showMyContactList()
    }
    
    func showMyContactList() {
        
        getSavedContacts()
        if savedContact.count > 0 {
            ui_tblContact.isHidden = false
        } else {
            ui_tblContact.isHidden = true
        }
        ui_tblContact.reloadData()
    }
    
    func setTextData() {
        EmergencyTitle.text = R.string.EMERGENCY_TITLE[0]
        ui_lblText_1D.text = R.string.EMERGENCY_TEXT1[0]
        ui_lblText_2D.text = R.string.EMERGENCY_TEXT2[0]
        ui_butAdd.setTitle(R.string.EMERGENCY_ADD[0], for: .normal)
    }
    
    @IBAction func onTapedAdd(_ sender: Any) {
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "MyContactsVC")
        
        self.navigationController?.pushViewController(toVC!, animated: true)
    }
    
}

extension EmergencyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedContact.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmergencyCell", for: indexPath) as! EmergencyCell
        
        cell.setCell(savedContact[indexPath.row].name!, savedContact[indexPath.row].number!)   //, contactModels[indexPath.row].avatar
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //self.setContact(indexPath.row)
    }
    
}
