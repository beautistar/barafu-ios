//
//  TravelTableViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import LGButton

class TripHistoryCollectionViewController: UICollectionViewController {
    //MARK: Properties
    let cellIdentifier = "TripHistoryCollectionViewCell"
    
    var travels = [Travel]()
    var colors = [[String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibCell = UINib(nibName: cellIdentifier, bundle: nil)
        collectionView?.register(nibCell, forCellWithReuseIdentifier: cellIdentifier)
        self.refreshList(self)
        colors.append(["#ff3e99","#ffa35a"])
        colors.append(["#668dff","#ff53ff"])
        colors.append(["#6ae0d7","#00d3ad"])
        configureCollectionViewLayout()
    }

    @IBAction func refreshList(_ sender: Any) {
        DriverSocketManager.shared.getTravels() { travels in
            self.travels = travels
            self.collectionView?.reloadData()
            //(sender as AnyObject).endRefreshing()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let kWhateverHeightYouWant = 110
        return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
    }
    
    override func numberOfSections(in tableView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return travels.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? TripHistoryCollectionViewCell  else {
            fatalError("The dequeued cell is not an instance of TripHistoryTableCell.")
        }
        // Fetches the appropriate meal for the data source layout.
        let travel = travels[indexPath.row]
        cell.pickupLabel.text = travel.pickupAddress
        cell.destinationLabel.text = travel.destinationAddress
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        
        if let startTimestamp = travel.startTimestamp {
            cell.startTimeLabel.text = dateFormatter.string(from: startTimestamp)
        }
        if let finishTimestamp = travel.finishTimestamp {
            cell.finishTimeLabel.text = dateFormatter.string(from: finishTimestamp)
        }
        style(cell: cell.rootView, index: indexPath.row)
        
        return cell
    }
    
    func style(cell:LGButton, index: Int) {
        cell.gradientStartColor = nil
        cell.gradientEndColor = nil
        cell.gradientStartColor = UIColor(hexString: colors[index % colors.count][0])
        cell.gradientEndColor = UIColor(hexString: colors[index % colors.count][1])
        cell.shadowColor = UIColor(hexString: colors[index % colors.count][0])
        cell.layer.shadowRadius = 8
        cell.layer.shadowOpacity = 0.8
    }
    
    private func configureCollectionViewLayout() {
        let itemHeight: CGFloat = 170
        let lineSpacing: CGFloat = 0
        let xInset: CGFloat = 0
        let topInset: CGFloat = 0
        guard let layout = collectionView?.collectionViewLayout as? VegaScrollFlowLayout else { return }
        layout.minimumLineSpacing = lineSpacing
        layout.sectionInset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        let itemWidth = UIScreen.main.bounds.width - 2 * xInset
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
