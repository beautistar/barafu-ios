//
//  EditProfileViewController.swift
//  Driver
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Eureka
import ImageRow
import Kingfisher

class EditProfileViewController: FormViewController {
    var downloading = false
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section(NSLocalizedString("profile.section.image", value: "Images", comment: "Profile's image section header"))
            <<< ImageRow() {
                $0.tag = "profile_row"
                $0.title = NSLocalizedString("profile.field.image", value: "Profile Image", comment: "Profile's image field title")
                $0.allowEditor = true
                $0.useEditedImage = true
                if let address = AppConfig.shared.user?.media?.address {
                    let url = URL(string: AppDelegate.info["ServerAddress"] as! String + address.replacingOccurrences(of: " ", with: "%20"))
                    ImageDownloader.default.downloadImage(with: url!){ result in
                        switch result {
                        case .success(let value):
                            self.downloading = true
                            (self.form.rowBy(tag: "profile_row")! as! ImageRow).value = value.image
                            (self.form.rowBy(tag: "profile_row")! as! ImageRow).reload()
                            self.downloading = false
                        case .failure(let error):
                            print(error)
                        }
                    }
                }
                $0.sourceTypes = .PhotoLibrary
                $0.clearAction = .no
                }.onChange {
                    if(!self.downloading) {
                        let data = $0.value?.jpegData(compressionQuality: 0.7)
                        $0.title = NSLocalizedString("profile.field.image.uploading", value: "Uploading, Please wait...", comment: "Uploading image state")
                        $0.disabled = true
                        $0.reload()
                        DriverSocketManager.shared.changeProfileImage(imageData: data!) { (res, media) in
                            self.form.rowBy(tag: "profile_row")!.title = NSLocalizedString("profile.field.image", value: "Profile Image", comment: "Profile's image field title")
                            self.form.rowBy(tag: "profile_row")!.disabled = false
                            self.form.rowBy(tag: "profile_row")!.reload()
                            if res != .OK {
                                DialogBuilder.alertOnResponse(response: res)
                                return
                            } else {
                                DialogBuilder.alertOnSuccess(message: NSLocalizedString("alert.success.upload", value: "Uploading Profile Image was successful.", comment: "Uploading profile image successful alert message"))
                            }
                            AppConfig.shared.user?.media? = Media(JSON: media!)!
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
                            let defaults:UserDefaults = UserDefaults.standard
                            defaults.set(encodedData, forKey:"settings")
                        }
                    }
                }
                .cellUpdate { cell, row in
                    cell.accessoryView?.layer.cornerRadius = 17
                    cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            }
            +++ Section(NSLocalizedString("profile.section.info", value: "Basic Info", comment: "Profile Basic Info section header"))
            <<< PhoneRow(){
                $0.title = NSLocalizedString("profile.field.mobile.number", value: "Mobile Number", comment: "Profile Mobile Number field title")
                $0.disabled = true
                $0.value = String(AppConfig.shared.user!.mobileNumber!)
            }
            <<< EmailRow(){
                $0.title = NSLocalizedString("profile.field.email", value: "E-Mail", comment: "Profile Email field title")
                $0.value = AppConfig.shared.user!.email
            }
            <<< TextRow(){
                $0.title = NSLocalizedString("profile.field.name", value: "Name", comment: "Profile Name field")
                $0.value = AppConfig.shared.user!.firstName
                $0.placeholder = NSLocalizedString("profile.field.name.first", value: "First Name", comment: "Profile First Name Field")
                }.onChange {
                    AppConfig.shared.user!.firstName = $0.value
            }
            <<< TextRow(){
                $0.title = " "
                $0.placeholder = NSLocalizedString("profile.field.name.last", value: "Last Name", comment: "Profile Last Name field")
                $0.value = AppConfig.shared.user!.lastName
                }.onChange {
                    AppConfig.shared.user!.lastName = $0.value
            }
            +++ Section(NSLocalizedString("profile.section.additional", value: "Additional Info", comment: "Profile's additional Info section"))
            <<< PushRow<String>() {
                $0.title = NSLocalizedString("profile.field.gender", value: "Gender", comment: "Profile's gender field title")
                $0.selectorTitle = NSLocalizedString("profile.field.gender.selector.title", value: "Select Your Gender", comment: "Profile's gender field selector title")
                $0.options = ["Male","Female","Unspecified"]
                $0.value = AppConfig.shared.user!.gender    // initially selected
                }.onChange {
                    AppConfig.shared.user!.gender = $0.value! as String
            }
            <<< TextRow(){
                $0.title = NSLocalizedString("profile.field.address", value: "Address", comment: "Profile Address field title")
                $0.value = AppConfig.shared.user!.address
                }.onChange { AppConfig.shared.user!.address = $0.value }
    }
    @IBAction func onSaveButtonClicked(_ sender: UIBarButtonItem) {
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: AppConfig.shared.user?.dictionaryRepresentation() as Any, options: .prettyPrinted),
            let theJSONText = String(data: theJSONData, encoding: String.Encoding.ascii) {
            DriverSocketManager.shared.editProfile(profileInfo: theJSONText) { (serverResponse) in
                if serverResponse != .OK {
                    DialogBuilder.alertOnResponse(response: serverResponse)
                    return
                }
                _ = self.navigationController?.popViewController(animated: true)
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
                let defaults:UserDefaults = UserDefaults.standard
                defaults.set(encodedData, forKey:"settings")
            }
        }
        
    }
}
