//
//  AboutUsVC.swift
//  rider
//
//  Created by RMS on 10/29/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var ui_lblTitleTop: UILabel!
    @IBOutlet weak var ui_lblContent1: UILabel!
    @IBOutlet weak var ui_lblContent2: UILabel!
    @IBOutlet weak var ui_lblContent3: UILabel!
    @IBOutlet weak var ui_lblContent4: UILabel!
    @IBOutlet weak var ui_lblContent5: UILabel!
    
    @IBOutlet weak var ui_lblTitleBottom: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUITextData()
    }
     
    func setUITextData() {
        
        ui_lblTitleTop.text = R.string.ABOUT_TOP[LANG_KEY]
        ui_lblContent1.text = R.string.THIS_IS_OUR_MOTTO[LANG_KEY]
        ui_lblContent2.text = R.string.WE_ARE_AN_INNOVATION[LANG_KEY]
        ui_lblContent3.text = R.string.SAFELY_TIME_AND_EXCELLENT[LANG_KEY]
        ui_lblContent4.text = R.string.WE_CALL_FOR_EVERY_RIDER[LANG_KEY]
        ui_lblContent5.text = R.string.WE_HAVE_A_DEDICATED[LANG_KEY]
        
        ui_lblTitleBottom.text = R.string.CONTACT_US_FOR[LANG_KEY]
        
    }
        
}
