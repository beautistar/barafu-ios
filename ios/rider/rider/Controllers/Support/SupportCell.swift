//
//  SupportCell.swift
//  rider
//
//  Created by RMS on 10/29/19.
//  Copyright © 2019 minimal. All rights reserved.
//
import UIKit
import Foundation

class SupportCell: UITableViewCell {
    
    @IBOutlet weak var ui_imgIcon: UIImageView!
    @IBOutlet weak var ui_lblIcon: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(_ imgName:String, _ title:String, _ state: Bool) {
        ui_imgIcon.image = UIImage(named: imgName)
        ui_lblIcon.text = title
        
        if state {
//            self.accessoryType = .disclosureIndicator
//            self.accessoryType = .detailDisclosureButton
//            self.selectionStyle = .blue
        }
    }

}
