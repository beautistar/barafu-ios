//
//  FavoriteAddressDialogViewController.swift
//  rider
//
//  Copyright © 1397 Minimalistic Apps. All rights reserved.
//

import UIKit
import GoogleMaps

class FavoriteAddressDialogViewController: UIViewController {
    @IBOutlet weak var textTitle: UITextField!
    @IBOutlet weak var textAddress: UITextField!
    @IBOutlet weak var map: GMSMapView!
    var address: Address?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if address != nil {
            textTitle.text = address?.title
            textAddress.text = address?.address
            map.animate(to: GMSCameraPosition.camera(withTarget: (address?.location)!, zoom: 18))
        } else {
            let locationManager = CLLocationManager()
            if let location = locationManager.location {
                map.animate(to: GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 18))
            }
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
