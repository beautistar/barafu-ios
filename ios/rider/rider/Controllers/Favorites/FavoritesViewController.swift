//
//  FavoritesViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import LGButton
import PopupDialog
import GoogleMaps

class FavoritesViewController: UICollectionViewController, FavoriteAddressDialogDelegate {
    func delete(address: Address) {
        RiderSocketManager.shared.crudAddress(crud: CRUD.DELETE, address: address) { result,_addresses in
            self.refreshList(self)
        }
    }
    
    func update(address: Address) {
        let dlg = FavoriteAddressDialogViewController(nibName: "FavoriteAddressDialogViewController", bundle: nil)
        dlg.address = address
        
        // Create the dialog
        let popup = PopupDialog(viewController: dlg, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "SAVE", height: 60) {
            dlg.address?.title = dlg.textTitle.text
            dlg.address?.address = dlg.textAddress.text
            dlg.address?.location = dlg.map.camera.target
            RiderSocketManager.shared.crudAddress(crud: CRUD.UPDATE, address: dlg.address!) { result,_addresses in
                self.refreshList(self)
            }
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    //MARK: Properties
    
    var addresses = [Address]()
    var colors = [[String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshList(self)
        colors.append(["#ff3e99","#ffa35a"])
        colors.append(["#668dff","#ff53ff"])
        colors.append(["#6ae0d7","#00d3ad"])
        
        configureCollectionViewLayout()
        //self.refreshList(self)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
    }
    
    @IBAction func onAddFavoriteClicked(_ sender: Any) {
        let dlg = FavoriteAddressDialogViewController(nibName: "FavoriteAddressDialogViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: dlg, buttonAlignment: .horizontal, transitionStyle: .bounceUp, tapGestureDismissal: false)
        
        // Create first button
        let buttonOne = CancelButton(title: NSLocalizedString("message.button.cancel",value: "Cancel", comment: "Message Cancel Button"), height: 60) {
            
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: NSLocalizedString("message.button.ok",value: "OK", comment: "Message OK button"), height: 60) {
            if let title = dlg.textTitle.text, title.isEmpty {
                DialogBuilder.alertOnError(message: "You need to enter a title for the location.")
                return
            }
            let address = Address()
            address.title = dlg.textTitle.text
            address.address = dlg.textAddress.text
            address.location = dlg.map.camera.target
            RiderSocketManager.shared.crudAddress(crud: CRUD.CREATE, address: address) { result,_addresses in
                self.refreshList(self)
                popup.dismiss()
            }
        }
        buttonTwo.dismissOnTap = false
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    @IBAction func refreshList(_ sender: Any) {
        RiderSocketManager.shared.crudAddress(crud: CRUD.READ, address: Address()) { response,addresses in
            self.addresses = addresses
            self.collectionView?.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addresses.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "AddressCell"
        
        guard let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? AddressCell  else {
            fatalError("The dequeued cell is not an instance of AddressCell.")
        }
        // Fetches the appropriate meal for the data source layout.
        let address = addresses[indexPath.row]
        cell.textTitle.text = address.title
        cell.address = address
        cell.delegate = self
        style(cell: cell.root, index: address.id!)
        
        return cell
    }
    
    func style(cell:LGButton, index: Int) {
        cell.gradientStartColor = UIColor(hexString: colors[index % colors.count][0])
        cell.gradientEndColor = UIColor(hexString: colors[index % colors.count][1])
        cell.shadowColor = UIColor(hexString: colors[index % colors.count][0])
        cell.layer.shadowRadius = 8
        cell.layer.shadowOpacity = 0.8
    }
    
    private func configureCollectionViewLayout() {
        let itemHeight: CGFloat = 70
        let lineSpacing: CGFloat = 0
        let xInset: CGFloat = 0
        let topInset: CGFloat = 0
        guard let layout = collectionView?.collectionViewLayout as? VegaScrollFlowLayout else { return }
        layout.minimumLineSpacing = lineSpacing
        layout.sectionInset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        let itemWidth = UIScreen.main.bounds.width - 2 * xInset
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
}
