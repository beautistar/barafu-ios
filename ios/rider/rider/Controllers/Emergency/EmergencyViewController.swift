//
//  EmergencyViewController.swift
//  rider
//
//  Created by RMS on 10/29/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit
import Foundation
import ContactsUI

var savedContact = [ContactModel]()

func getSavedContacts() {
    if let data = UserDefaults.standard.data(forKey: "emg_contacts"),
        let contactdata = NSKeyedUnarchiver.unarchiveObject(with: data) as? [ContactModel] {
        savedContact = contactdata
    } else {
        print("error")
    }
}

class EmergencyViewController: UIViewController {

    @IBOutlet weak var ui_emergencyTitle: UILabel!
    @IBOutlet weak var ui_lblText_1R: UILabel!
    @IBOutlet weak var ui_lblText_2R: UILabel!
    
    @IBOutlet weak var ui_butAddR: UIButton!
    @IBOutlet weak var ui_tblContact: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setTextData()
        ui_tblContact.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showMyContactList()
    }
    
    func setTextData() {
        ui_emergencyTitle.text = R.string.EMERGENCY_TOP[0]
        ui_lblText_1R.text = R.string.EMERGENCY_TEXT1[0]
        ui_lblText_2R.text = R.string.EMERGENCY_TEXT2[0]
        
        ui_butAddR.setTitle(R.string.EMERGENCY_ADD[0], for: .normal)
    }
    
    func showMyContactList() {
        
        getSavedContacts()
        if savedContact.count > 0 {
            ui_tblContact.isHidden = false
        } else {
            ui_tblContact.isHidden = true
        }
        ui_tblContact.reloadData()
    }
    
//    func fetchMyEmgContact() {
//        
//        if let data = UserDefaults.standard.data(forKey: "emg_contacts") ,
//            let contactdata = NSKeyedUnarchiver.unarchiveObject(with: data) as? [ContactModel] {
//            contactModels = contactdata
//            contactModels.forEach({print( $0.name!, $0.numbers!)})
//
//        } else {
//            print("error")
//        }
//    }
//    
//    func fetchContact() {
//      
//        let contactStore = CNContactStore()
//        let keysToFetch = [
//            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
//            CNContactPhoneNumbersKey,
//            CNContactEmailAddressesKey,
//            CNContactThumbnailImageDataKey] as [Any]
//
//        var allContainers: [CNContainer] = []
//        do {
//            allContainers = try contactStore.containers(matching: nil)
//        } catch {
//            print("Error fetching containers")
//        }
//
//        var results: [CNContact] = []
//
//        for container in allContainers {
//            
//            print("container", container)
//            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
//
//            do {
//                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
//                print("containerResults===", containerResults)
//                results.append(contentsOf: containerResults)
//                
//            } catch {
//                print("Error fetching containers")
//            }
//        }
//        
//        for contact in results {
//            
//            var numbers = [String]()
//            for one in contact.phoneNumbers {
//                numbers.append(one.value.stringValue)
//            }
//            
//            let contactModel = ContactModel(name: contact.givenName, numbers: numbers)
//            contactModels.append(contactModel)
//        }
//        
//        let encodedData = NSKeyedArchiver.archivedData(withRootObject: contactModels)
//        UserDefaults.standard.set(encodedData, forKey: "emg_contacts")
//        UserDefaults.standard.synchronize()
//    }
//    
//   
    
    @IBAction func ui_butAddContact(_ sender: Any) {
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "MyContactsVC")
        
        self.navigationController?.pushViewController(toVC!, animated: true)
    }
    
}


extension EmergencyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedContact.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmergencyCell", for: indexPath) as! EmergencyCell
        
        cell.setCell(savedContact[indexPath.row].name!, savedContact[indexPath.row].number!)   //, contactModels[indexPath.row].avatar
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //self.setContact(indexPath.row)
    }
    
}

