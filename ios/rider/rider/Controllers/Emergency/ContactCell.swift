//
//  ContactCell.swift
//  rider
//
//  Created by RMS on 11/1/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import Foundation
import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var ui_imgAvatar: UIImageView!
    @IBOutlet weak var ui_lblUsername: UILabel!
    @IBOutlet weak var ui_lblPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //, _ imgName: String
    func setCell(_ username: String, _ phone:String) {
//        ui_imgIcon.image = UIImage(named: imgName)
        ui_lblUsername.text = username
        ui_lblPhone.text = phone
        
    }
}
