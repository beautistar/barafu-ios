//
//  PromotionsCollectionViewController.swift
//  rider
//
//  Created by Manly Man on 11/23/18.
//  Copyright © 2018 minimal. All rights reserved.
//

import UIKit
import LGButton

class PromotionsCollectionViewController: UICollectionViewController {//MARK: Properties
    let cellIdentifier = "PromotionsCollectionViewCell"
    var promotions = [Promotion]()
    var colors = [[String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibCell = UINib(nibName: cellIdentifier, bundle: nil)
        collectionView?.register(nibCell, forCellWithReuseIdentifier: cellIdentifier)
        self.refreshList(self)
        colors.append(["#ff3e99","#ffa35a"])
        colors.append(["#668dff","#ff53ff"])
        colors.append(["#6ae0d7","#00d3ad"])
        configureCollectionViewLayout()
    }
    
    @IBAction func refreshList(_ sender: Any) {
        RiderSocketManager.shared.getPromotions() { promotions in
            self.promotions = promotions
            self.collectionView?.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotions.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? PromotionsCollectionViewCell  else {
            fatalError("The dequeued cell is not an instance of PromotionsCollectionViewCell.")
        }
        // Fetches the appropriate meal for the data source layout.
        let promotion = promotions[indexPath.row]
        cell.promotion = promotion
        style(cell: cell.rootView, index: indexPath.row)
        
        return cell
    }
    
    func style(cell:LGButton, index: Int) {
        cell.gradientStartColor = nil
        cell.gradientEndColor = nil
        cell.gradientStartColor = UIColor(hexString: colors[index % colors.count][0])
        cell.gradientEndColor = UIColor(hexString: colors[index % colors.count][1])
        cell.shadowColor = UIColor(hexString: colors[index % colors.count][0])
        cell.layer.shadowRadius = 8
        cell.layer.shadowOpacity = 0.8
    }
    
    private func configureCollectionViewLayout() {
        let itemHeight: CGFloat = 107
        let lineSpacing: CGFloat = 0
        let xInset: CGFloat = 0
        let topInset: CGFloat = 0
        guard let layout = collectionView?.collectionViewLayout as? VegaScrollFlowLayout else { return }
        layout.minimumLineSpacing = lineSpacing
        layout.sectionInset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        let itemWidth = UIScreen.main.bounds.width - 2 * xInset
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
}
