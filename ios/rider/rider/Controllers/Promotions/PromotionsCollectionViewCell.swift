//
//  TripHistoryCollectionViewCell.swift
//  rider
//
//  Copyright © 2018 minimal. All rights reserved.
//

import UIKit
import LGButton

class PromotionsCollectionViewCell: UICollectionViewCell {
    public var promotion: Promotion?
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var textdescription: UILabel!
    @IBOutlet weak var rootView: LGButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        rootView.gradientStartColor = nil
        rootView.gradientEndColor = nil
        rootView.shadowColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let pr = promotion {
            title.text = pr.title
            textdescription.text = pr.descriptionValue
        }
    }
}
