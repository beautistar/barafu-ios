//
//  TripHistoryCollectionViewCell.swift
//  rider
//
//  Copyright © 2018 minimal. All rights reserved.
//

import UIKit
import LGButton

class CouponsCollectionViewCell: UICollectionViewCell {
    public var coupon: Coupon?
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var textdescription: UILabel!
    @IBOutlet weak var rootView: LGButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        rootView.gradientStartColor = nil
        rootView.gradientEndColor = nil
        rootView.shadowColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if coupon != nil {
            title.text = coupon?.title
            textdescription.text = coupon?.descriptionValue
        }
    }
}
