//
//  TravelViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Cosmos
import GoogleMaps
import GooglePlaces
import PopupDialog
import MTSlideToOpen
import RMPickerViewController

import Kingfisher
import KingfisherWebP
import SwiftyJSON

class TravelViewController: UIViewController,MTSlideToOpenDelegate, CouponsViewDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var labelBalance: UILabel!
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    var driverMarker = GMSMarker()
    var riderMarker = GMSMarker()
    var isTravelStarted = false
    var driverLocation  = CLLocationCoordinate2D()
    var polyline = GMSPolyline()
    @IBOutlet weak var slideCancel: MTSlideToOpenView!
    
    @IBOutlet weak var ui_imgAvatar: UIImageView!
    @IBOutlet weak var ui_lblUsername: UILabel!

    
    let server_url = "http://3.130.44.140:8080/"
    var dirverInfo = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onServiceStarted), name: .serviceStarted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onServiceCanceled), name: .serviceCanceled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onServiceFinished), name: .serviceFinished, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onTravelInfoReceived), name: .travelInfoReceived, object: nil)
        //mapView.camera = camera
        driverMarker.icon = #imageLiteral(resourceName: "marker taxi")
        riderMarker.icon = UIImage(named: "marker_pickup")
        self.navigationItem.hidesBackButton = true
        slideCancel.sliderViewTopDistance = 6
        slideCancel.sliderCornerRadius = 15
        slideCancel.delegate = self
        slideCancel.defaultLabelText = "Slide To Cancel"
        slideCancel.thumnailImageView.image = #imageLiteral(resourceName: "cross")
//        slideCancel.thumnailImageView.image = #imageLiteral(resourceName: "menuLocations")
        
        if var cost = Travel.shared.costBest {
            if let coupon = Travel.shared.coupon {
                cost = cost * Double(100 - coupon.discountPercent!) / 100
                cost = cost - Double(coupon.discountFlat!)
                
                self.labelCost.text = formattedNumber(Float(cost).rounded()) + currencyUnit[CURRENCY_KEY]
                
                
//                self.labelCost.text = String(format: "%.0f " + currencyUnit[CURRENCY_KEY], cost)
            } else {
                labelCost.text = formattedNumber(Float(cost).rounded()) + currencyUnit[CURRENCY_KEY]
//                labelCost.text = String(format: "%.0f " + currencyUnit[CURRENCY_KEY], cost)
            }
        }
        if let balance = AppConfig.shared.user?.balance {
            
            labelBalance.text = formattedNumber(Float(balance).rounded()) + currencyUnit[CURRENCY_KEY]
            
//            labelBalance.text = String(format: "%.0f " + currencyUnit[CURRENCY_KEY], balance)
        }
    }

    @IBAction func onRightBarButtonClicked(_ sender: UIBarButtonItem) {
        if isTravelStarted {
            showReviewDialog()
        } else {
            showContactModeChoose()
        }
    }
    
    func drawMapPath(origin: String, destination: String) {

        //Setting the start and end location
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
        let dic = NSDictionary(contentsOfFile: path) as! [String: Any]
         GMSPlacesClient.provideAPIKey(dic["GoogleMapsAPIKey"] as! String)
        
        let urls = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(dic["GoogleMapsAPIKey"] as! String)"
        
        let url = URL(string: urls)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in

            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    print(json)
                    let routes = json["routes"] as! NSArray
                    

                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            self.polyline = GMSPolyline.init(path: path)
                            self.polyline.strokeColor = .black
                            self.polyline.strokeWidth = 3

                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))

                            self.polyline.map = self.mapView

                        }
                    })
                } catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()
    }
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        RiderSocketManager.shared.cancelService() { serverResponse in
            if serverResponse == ServerResponse.OK {
                let title = "Message"
                let message = "Service Has Been Canceled Successfully."
                
                // Create the dialog
                let popup = PopupDialog(title: title, message: message)
                
                // Create buttons
                let buttonOne = DefaultButton(title: "OK") {
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
                popup.addButtons([buttonOne])
                self.present(popup, animated: true, completion: nil)
            } else {
                let title = "Message"
                let message = "Service cancellation was unsuccessful for some reason. try again."
                
                // Create the dialog
                let popup = PopupDialog(title: title, message: message)
                
                // Create buttons
                let buttonOne = DefaultButton(title: "OK") {
                    
                }
                popup.addButtons([buttonOne])
                self.present(popup, animated: true, completion: nil)
            }
        }
    }
    
    @objc func onServiceStarted(_ notification: Notification) {
        isTravelStarted = true
        riderMarker.icon = UIImage(named: "marker_destination")
        slideCancel.isHidden = true
        self.navigationItem.title = "Travel started"
        self.navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "favorites")
        updateMarkers()
        
        print("rms_ onserviceStarted : ", notification)
    }
    
    @objc func onServiceCanceled(_ notification: Notification){
        let title = "Message"
        let message = "Service Has Been Canceled by other party. You won't be charged for this trip."
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = DefaultButton(title: "OK") {
            _ = self.navigationController?.popViewController(animated: true)
        }

        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func onServiceFinished(_ notification: Notification){
        let title = "Finished!"
        var message = ""
        let obj = notification.object as! [Any]
        
        if (obj[1] as! Bool) {
            message = String(format: "Travel Has been finished and amount of %.0f$ has been withdrawn from your credit", obj[2] as! Double)
        } else {
            message = String(format: "Travel Has been finished There was not sufficient amount of credit to do so. pay %.0f$ in cash!", obj[2] as! Double)
        }
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = DefaultButton(title: "OK") {
            self.navigationController?.popViewController(animated: true)
        }
        
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
       
    }
    
    @objc func onTravelInfoReceived(_ notification: Notification){
        let obj = notification.object as! [Any]
        Travel.shared.distanceReal = obj[0] as? Int
        Travel.shared.durationReal = obj[1] as? Int
        let lat = obj[3] as! Double
        let lng = obj[4] as! Double
        driverLocation = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        labelTime.text = String(format: "%02d':%02d\"",Travel.shared.durationReal! / 60, Travel.shared.durationReal! % 60)
        labelDistance.text = String(format: "%.1f km", Double(Travel.shared.distanceReal!) / 1000)
        updateMarkers()
        print("rms_onTravelInfoReceived : ", Travel.shared.driver)
        
        
        gotoAPIDirverData(driverId : Travel.shared.driver!.id!)
    }
    
    func updateMarkers() {
        driverMarker.map = mapView
        riderMarker.map = mapView
        driverMarker.position = driverLocation
        if isTravelStarted {
            riderMarker.position = (Travel.shared.destinationPoint)!
            let startPoint = "\(Travel.shared.pickupPoint!.latitude),\(Travel.shared.pickupPoint!.longitude)"
            let endPoint = "\(Travel.shared.destinationPoint!.latitude),\(Travel.shared.destinationPoint!.longitude)"
            drawMapPath(origin: startPoint, destination: endPoint)
        } else {
            riderMarker.position = (Travel.shared.pickupPoint)!
            polyline.map = nil
        }
        let bounds = GMSCoordinateBounds(coordinate: riderMarker.position, coordinate: driverMarker.position)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        mapView.animate(with: update)
        
    }
    
    func gotoAPIDirverData(driverId : Int){
        
        ApiManager.getDriverInfoGet(driverId: driverId) { (isSuccess, data) in
                    
            if isSuccess {
                self.dirverInfo = JSON(data!)
                self.setDriverInfo(self.dirverInfo)
            } else {
                if data == nil {
                    //self.showToast(R.string.CONNECT_FAIL[LANG_KEY])
                } else {
                    //self.showToast(JSON(data!).stringValue)
                }
            }
        }
    }
    
    func setDriverInfo(_ dirverInfo : JSON) {
        
        if dirverInfo["media_url"].stringValue != "" {
            let image = dirverInfo["media_url"].stringValue
            let driverImage = server_url + image.replacingOccurrences(of: " ", with: "%20")
            let url = URL(string: driverImage)
            
            let processor = DownsamplingImageProcessor(size: ui_imgAvatar.intrinsicContentSize) >> RoundCornerImageProcessor(cornerRadius: ui_imgAvatar.intrinsicContentSize.width / 2)
            
            
            ui_imgAvatar.kf.setImage(with: url, placeholder: UIImage(named: "Nobody"), options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5)),
                .cacheOriginalImage
            ]) { result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
        ui_lblUsername.text = dirverInfo["first_name"].stringValue + " " + dirverInfo["last_name"].stringValue
        
    }
    
    @objc func back(sender: UIBarButtonItem) {
        let title = "Cancel Travel"
        let message = "Are you sure you want to cancel travel?"
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: "CANCEL") {
            popup.dismiss()
        }
        
        let buttonTwo = DefaultButton(title: "YES") {
            RiderSocketManager.shared.cancelService() { data in
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
        popup.addButtons([buttonOne, buttonTwo])
        self.present(popup, animated: true, completion: nil)
    }
    
    func showReviewDialog(animated: Bool = true) {
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            //self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "RATE", height: 60) {
            RiderSocketManager.shared.reviewDriver(score: ratingVC.cosmosStarRating.rating * 20, review: ratingVC.commentTextField.text!) { data in
                if data.rawValue != 200 {
                    DialogBuilder.alertOnResponse(response: data)
                } else {
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
            }
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: animated, completion: nil)
    }
    
    @IBAction func onSelectCouponClicked(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CouponsCollectionViewController") as? CouponsCollectionViewController
        {
            vc.selectMode = true
            vc.delegate = self
            present(vc, animated: true)
        }
    }
    
    func didSelectedCoupon(_ coupon: Coupon) {
        RiderSocketManager.shared.applyCoupon(code: coupon.code!) { response in
            DialogBuilder.alertOnResponse(response: response)
            var cost = Travel.shared.costBest!
            cost = cost * Double(100 - coupon.discountPercent!) / 100
            cost = cost - Double(coupon.discountFlat!)
            
            self.labelCost.text = formattedNumber(Float(cost).rounded()) + currencyUnit[CURRENCY_KEY]
            
//            self.labelCost.text = String(format: NSLocalizedString("base.money", value: "%.0f " + currencyUnit[CURRENCY_KEY], comment: "Default Currency format. 0 stands for digits after '.'"), cost)
        }
    }
    
    func showContactModeChoose() {
        let style = RMActionControllerStyle.white
        let title = NSLocalizedString("travel.picker.contact.title", value: "Contact Method", comment: "Contact Method picker title")
        let selectAction = RMAction<UIPickerView>(title: NSLocalizedString("message.button.select",value: "Select", comment: "Message Select Button"), style: .done) { controller in
            if controller.contentView.selectedRow(inComponent: 0) == 0 {
                self.directCall()
            } else {
                self.requestCall()
            }
        }
        
        let cancelAction = RMAction<UIPickerView>(title: NSLocalizedString("message.button.cancel",value: "Cancel", comment: "Message Cancel Button"), style: .cancel) { _ in
            print("Row selection was canceled")
        }
        
        let actionController = RMPickerViewController(style: style, title: title, message: NSLocalizedString("travel.picker.contact.description", value: "Select a contact method", comment: "Contact method picker description"), select: selectAction, andCancel: cancelAction)!;
        
        actionController.picker.delegate = self;
        actionController.picker.dataSource = self;
        
        if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            actionController.modalPresentationStyle = UIModalPresentationStyle.popover
        }
        self.present(actionController, animated: true, completion: nil)
    }
    
    func directCall() {
        
        if dirverInfo["mobile_number"].intValue != 0 {
            if let url = URL(string: "tel://\(dirverInfo["mobile_number"].intValue)"),
            UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    // Fallback on earlier versions
                    let dialog = DialogBuilder.getDialogForMessage(message: NSLocalizedString("message.call.request.sent", value: "You can not call as  you haven't any phone number.", comment: ""), completion: nil)
                    self.present(dialog, animated: true, completion: nil)
                }
            }
        }
      
    }
    
    func requestCall() {
        RiderSocketManager.shared.callRequest() { data in
            if data.rawValue != 200 {
                DialogBuilder.alertOnResponse(response: data)
            } else {
                let dialog = DialogBuilder.getDialogForMessage(message: NSLocalizedString("message.call.request.sent", value: "You can not call as  you haven't any phone number.", comment: ""), completion: nil)
                self.present(dialog, animated: true, completion: nil)
            }
        }
    }
}
extension TravelViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    // MARK: UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return NSLocalizedString("travel.contact.direct", value: "Direct Call", comment: "Contact Method Direct. Used in picker.")
        } else {
            return NSLocalizedString("travel.contact.request", value: "Request Call", comment: "Contact Method Request. Used in picker.")
        }
    }
}

