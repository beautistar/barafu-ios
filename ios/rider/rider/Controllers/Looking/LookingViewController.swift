//
//  LookingViewController.swift
//  rider
//
//  Copyright © 2019 minimalistic apps. All rights reserved.
//

import UIKit
import LGButton
import Lottie

class LookingViewController: UIViewController {
    @IBOutlet weak var ViewLoading: UIView!
    var animationView: AnimationView!
    weak var delegate: LookingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.onDriverAccepted), name: .newDriverAccepted, object: nil)
        
        animationView = AnimationView(name: "car")
        animationView.contentMode = .scaleAspectFit
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.loopMode = .loop
        ViewLoading.addSubview(animationView)
        // Do any additional setup after loading the view.
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        let horizontalConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 0)
        ViewLoading.addConstraints([horizontalConstraint,verticalConstraint,widthConstraint,heightConstraint])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animationView.play()
    }
    
    @IBAction func onCancelRequestClicked(_ sender: LGButton) {
        RiderSocketManager.shared.cancelRequest()
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onDriverAccepted(_ notification: Notification) {
        if let myDict = notification.object as? [String: Any] {
            Travel.shared.driver = myDict["driver"] as? Driver
            dismiss(animated: true, completion: nil)
            self.delegate?.found()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}

protocol LookingDelegate: class{
    func found()
    func cancel()
}
