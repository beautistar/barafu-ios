//
//  NavigationMenuViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import InteractiveSideMenu
import Kingfisher
import KingfisherWebP

class NavigationMenuViewController : MenuViewController {
    let kCellReuseIdentifier = "MenuCell"
    let menuItems = ["Main"]
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCredit: UILabel!
    
    @IBOutlet weak var ui_butTrip: UIButton!
    @IBOutlet weak var ui_butprofile: UIButton!
    @IBOutlet weak var ui_butLcation: UIButton!
    @IBOutlet weak var ui_butWallet: UIButton!
    @IBOutlet weak var ui_butCoupon: UIButton!
    @IBOutlet weak var ui_butPromotion: UIButton!
    @IBOutlet weak var ui_butTrancation: UIButton!
    @IBOutlet weak var ui_butSuppot: UIButton!
    @IBOutlet weak var ui_butEmergency: UIButton!
    @IBOutlet weak var ui_butAbout: UIButton!
    @IBOutlet weak var ui_butLogout: UIButton!
    
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageUser.layer.cornerRadius = imageUser.frame.size.width / 2
        imageUser.clipsToBounds = true
        imageUser.layer.borderColor = UIColor.white.cgColor
        imageUser.layer.borderWidth = 3.0
        
        updateUILanguage()
    }
    
    func updateUILanguage() {
        
        //textLoading.text = R.string.LOADING_MARK[LANG_KEY]
        
        ui_butTrip.setTitle(R.string.BUT_TRIPHISTORY[LANG_KEY], for: .normal)
        ui_butprofile.setTitle(R.string.BUT_YOURPROFILE[LANG_KEY], for: .normal)
        ui_butLcation.setTitle(R.string.BUT_LOCATION[LANG_KEY], for: .normal)
        ui_butWallet.setTitle(R.string.BUT_WALLET[LANG_KEY], for: .normal)
        ui_butCoupon.setTitle(R.string.BUT_COUPONS[LANG_KEY], for: .normal)
        ui_butPromotion.setTitle(R.string.BUT_PORMOTIONS[LANG_KEY], for: .normal)
        ui_butTrancation.setTitle(R.string.BUT_TRANSACTIONS[LANG_KEY], for: .normal)
        ui_butSuppot.setTitle(R.string.BUT_SUPPORT[LANG_KEY], for: .normal)
        ui_butEmergency.setTitle(R.string.BUT_EMERGENCY[LANG_KEY], for: .normal)
        ui_butAbout.setTitle(R.string.BUT_ABOUT[LANG_KEY], for: .normal)
        ui_butLogout.setTitle(R.string.BUT_LOGOUT[LANG_KEY], for: .normal)
        
        
        self.view.updateFocusIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let riderImage = AppConfig.shared.user?.media?.address {
            let processor = DownsamplingImageProcessor(size: imageUser.intrinsicContentSize) >> RoundCornerImageProcessor(cornerRadius: imageUser.intrinsicContentSize.width / 2)
            let url = URL(string: AppDelegate.info["ServerAddress"] as! String + riderImage.replacingOccurrences(of: " ", with: "%20"))
            imageUser.kf.setImage(with: url, placeholder: UIImage(named: "Nobody"), options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5)),
                .cacheOriginalImage
            ]) { result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
        
        if let user = AppConfig.shared.user {
            labelName.text = "\(user.firstName == nil ? "" : user.firstName!) \(user.lastName == nil ? "" : user.lastName!)"
        }
        if let balance = AppConfig.shared.user?.balance {
            
            labelCredit.text = R.string.T_BALANCE[LANG_KEY]  + " : " +  formattedNumber(Float(balance).rounded()) + currencyUnit[CURRENCY_KEY]
            
//            labelCredit.text = String(format: R.string.T_BALANCE[LANG_KEY] + ": %.0f " + currencyUnit[CURRENCY_KEY], balance)
            
        }
    }
    
    @IBAction func onTravelsClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showTravels", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onProfileClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showEditProfile", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onFavoritesClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showFavorites", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onWalletClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
       
        
        //    showWallet
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showWalletCard", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onCouponsClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showCoupons", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onPromotionsClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showPromotions", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onTransactionsClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showTransactions", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onSupportCilcked(_ sender: Any) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showSupport", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onEmergency(_ sender: Any) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showEmergency", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onAboutClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.contentViewControllers[0].performSegue(withIdentifier: "showAbout", sender: nil)
        menuContainerViewController.hideSideMenu()
    }
    
    @IBAction func onExitClicked(_ sender: UIButton) {
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        if let bundle = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundle)
        //self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            
            menuContainerViewController.hideSideMenu()
        }
    }
}
