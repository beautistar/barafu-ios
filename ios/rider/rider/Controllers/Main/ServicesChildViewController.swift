//
//  ServicesChildViewController.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import LGButton
import PopupDialog

class ServicesChildViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    public var serviceCategoryIndex: Int?
    public var serviceIndex: Int?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonSelectService: LGButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        disableConfirm()
        
        buttonSelectService.titleString = R.string.B_CONFIRM[LANG_KEY]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let services = ServiceCategory.lastDownloaded[serviceCategoryIndex!].services else {
            return 0
        }
        return (services.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serviceCell", for: indexPath) as! ServicesListCell
        cell.update(service: (ServiceCategory.lastDownloaded[serviceCategoryIndex!].services?[indexPath.item])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.enableConfirm()
        buttonSelectService.titleString = "Confirm " + ServiceCategory.lastDownloaded[serviceCategoryIndex!].services![indexPath.item].title!
        serviceIndex = indexPath.item
    }
    @IBAction func onSelectServiceClicked(_ sender: LGButton) {
        Travel.shared.costBest = ServiceCategory.lastDownloaded[serviceCategoryIndex!].services![serviceIndex!].cost
        NotificationCenter.default.post(name: .startTravel, object: ServiceCategory.lastDownloaded[serviceCategoryIndex!].services![serviceIndex!].id)
    }
    
    func enableConfirm(){
        buttonSelectService.isEnabled = true
        buttonSelectService.bgColor = UIColor(hexString: "FFAC08")
//        buttonSelectService.shadowOpacity = 0.7
        
    }
    
    func disableConfirm() {
        buttonSelectService.isEnabled = false
        buttonSelectService.bgColor = UIColor(hexString: "939393")
//        buttonSelectService.shadowOpacity = 00
    }
    
}
