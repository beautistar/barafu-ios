//
//  ServicesViewController.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class ServicesParentViewController: TabmanViewController,PageboyViewControllerDataSource,TMBarDataSource {
    private var viewControllers = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        // Create bar
        let bar = TMBar.ButtonBar()
        
        // Add to view
        addBar(bar, dataSource: self, at: .top)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        let category = ServiceCategory.lastDownloaded[index]
        return TMBarItem(title: category.title!)
    }
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        viewControllers = [UIViewController]()
        for _ in ServiceCategory.lastDownloaded {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "servicesChild") as! ServicesChildViewController
            viewControllers.append(viewController)
        }
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        let viewController:ServicesChildViewController = (self.viewControllers[index] as! ServicesChildViewController);
        viewController.serviceCategoryIndex = index
        return viewController
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
}
