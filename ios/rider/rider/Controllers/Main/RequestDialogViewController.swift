//
//  RequestDialogViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Lottie
import Koloda
import Kingfisher
import GoogleMaps

class RequestDialogViewController: UIViewController {
    
    @IBOutlet weak var cardsStack: KolodaView!
    @IBOutlet weak var textLoading: UILabel!
    @IBOutlet weak var ViewLoading: UIView!
    var animationView: AnimationView!
    var driverInfos: [DriverInfo] = []
    public var dialogResult: RequestTaxiResultEvent!
    public var selectedServiceId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onDriverAccepted), name: .newDriverAccepted, object: nil)
        animationView = AnimationView(name: "PinJump")
        animationView.contentMode = .scaleAspectFit
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.loopMode = .loop
        ViewLoading.addSubview(animationView)
        self.cardsStack.dataSource = self
        self.cardsStack.delegate = self
        let horizontalConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: animationView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: ViewLoading, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 0)
        ViewLoading.addConstraints([horizontalConstraint,verticalConstraint,widthConstraint,heightConstraint])
        RiderSocketManager.shared.requestTaxi(originLocation: (Travel.shared.pickupPoint)!, destinationLocation: (Travel.shared.destinationPoint)!, originAddress: (Travel.shared.pickupAddress)!, destinationAddress: (Travel.shared.destinationAddress)!, serviceId: self.selectedServiceId!) { result in
            let result = result as RequestTaxiResultEvent
            if result.hasError() {
                self.dialogResult = result
                self.dismiss(animated: true, completion: nil)
                return
            }
            self.textLoading.text = String(format: "Request has been sent to %d drivers", arguments: [result.driversSentTo])
            
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        animationView.play()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func onDriverAccepted(_ notification: Notification){
        if let myDict = notification.object as? [String: Any] {
            let driverInfo = DriverInfo()
            driverInfo.driver = myDict["driver"] as? Driver
            driverInfo.distance = myDict["distance"] as? Int
            driverInfo.duration = (myDict["duration"] as! Int)
            driverInfo.cost = myDict["cost"] as? Double
            Travel.shared.costBest = driverInfo.cost
            driverInfos.append(driverInfo)
            cardsStack.isHidden = false
            cardsStack.reloadData()
        }
    }
    
}
extension RequestDialogViewController: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        koloda.reloadData()
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        //UIApplication.shared.openURL(URL(string: "https://yalantis.com/")!)
    }
}

extension RequestDialogViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return driverInfos.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        //return DriverRequestCard(driver: drivers[index], distance: 0, duration: 0)!
        let vc = Bundle.main.loadNibNamed("DriverRequestCard", owner: self, options: nil)?[0] as! DriverRequestCard
        vc.imageDriver.layer.cornerRadius = vc.imageDriver.frame.size.width / 2
        vc.imageDriver.clipsToBounds = true
        vc.imageDriver.layer.borderColor = UIColor.white.cgColor
        vc.imageDriver.layer.borderWidth = 3.0
        let info = driverInfos[Int(index)]
        vc.labelName.text = info.driver?.lastName
        if let driverImage = info.driver?.media?.address {
            vc.imageDriver.kf.setImage(with: URL(string: AppDelegate.info["ServerAddress"] as! String + driverImage.replacingOccurrences(of: " ", with: "%20")))
        }
        if let carImage = info.driver?.car?.media?.address {
            vc.imageHeader.kf.setImage(with: URL(string: AppDelegate.info["ServerAddress"] as! String + carImage))
        }
        vc.labelCar.text = info.driver?.car?.title
        vc.labelDistance.text = String(format: "%.1f km", Double(info.distance!) / 1000.0)
        let min = info.duration! / 60
        let sec = info.duration! % 60
        vc.labelDuration.text = String(format: "%02d':%02d\"", arguments: [min,sec])
        vc.driverId = (info.driver?.id!)!
        if let cost = info.cost {
            vc.labelCost.text = formattedNumber(Float(cost).rounded()) + currencyUnit[CURRENCY_KEY]
//            vc.labelCost.text = String(format: "%.0f " + currencyUnit[CURRENCY_KEY], cost)
        } else {
            vc.labelCost.text = "-"
        }
        if let rating = info.driver?.rating {
            vc.labelRating.text = String(format: "%.1f %", rating)
        } else {
            vc.labelRating.text = "-"
        }
        vc.buttonAccept.addTarget(self, action: #selector(self.acceptClicked), for: .touchUpInside)
        //return UIImageView(image: nil)
        return vc
    }
    @objc func acceptClicked(){
        self.cardsStack.isHidden = true
        RiderSocketManager.shared.acceptDriver(driverId: (driverInfos[cardsStack.currentCardIndex].driver?.id!)!)
        let res = RequestTaxiResultEvent.init(code: 200, driversSentTo: 0)
        self.dialogResult = res
        self.dismiss(animated: true, completion: nil)
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("DriverRequestCard", owner: self, options: nil)?[0] as? OverlayView
    }
}
