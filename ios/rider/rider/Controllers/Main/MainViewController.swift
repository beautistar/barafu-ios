//
//  MainViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import PopupDialog
import LGButton
import Tabman
import MarqueeLabel
import RMPickerViewController

class MainViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var map: GMSMapView!
    var markerPickup = GMSMarker()
    var markerDropOff = GMSMarker()
    var pickUpLocation = CLLocationCoordinate2D()
    var destinationLocation = CLLocationCoordinate2D()
    var currentLocation = CLLocationCoordinate2D()
    var arrayDriversMarkers: [GMSMarker] = []
    enum MarkerMode {
        case origin, destination, services
    }
    var markerMode = MarkerMode.origin
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var locationManager = CLLocationManager()
    var servicesViewController: ServicesParentViewController?
    
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var buttonConfirmLocation: LGButton!
    @IBOutlet weak var containerServices: UIView!
    @IBOutlet weak var locationLabel: MarqueeLabel!
    @IBOutlet weak var imageMarker: UIImageView!
    @IBOutlet weak var leftBarButton: UIButton!
    @IBOutlet weak var buttonCurrentLocation: UIButton!
    @IBOutlet weak var buttonFavorites: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map.padding = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        map.delegate = self
        map.isMyLocationEnabled = true
        map.settings.myLocationButton = true
        
        self.disableConfirm()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.onStartTravelRequested), name: .startTravel, object: nil)
        drawSearchBarShadow()
        let tap = UITapGestureRecognizer(target: self, action: #selector(MainViewController.tapLocation))
        locationLabel.addGestureRecognizer(tap)
        RiderSocketManager.shared.getStatus() { response,travel in
            if response == .NOT_FOUND {
                return
            }
            let title = "Message"
            let message = "There is an unfinished travel found. Upon tapping OK you will be redirected to Travel screen."
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message)
            
            // Create buttons
            let buttonOne = DefaultButton(title: "OK") {
                Travel.shared = travel
                self.performSegue(withIdentifier: "startTravel", sender: nil)
            }
            let buttonTwo = CancelButton(title: "Cancel") {
                popup.dismiss()
            }
            
            popup.addButtons([buttonOne,buttonTwo])
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isTranslucent = true
        super.viewWillAppear(animated)
        
        updateUILanguage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
        map.clear()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ServicesParentViewController,
            segue.identifier == "segueServices" {
            self.servicesViewController = vc
        }
        if let vc = segue.destination as? LookingViewController, segue.identifier == "startLooking" {
            vc.delegate = self
        }
    }
    
    func updateUILanguage() {
        buttonConfirmLocation.titleString = NSLocalizedString("main.button.confirm.pickup",value: R.string.PICKUP_1_BUT[LANG_KEY], comment: "Main Page Confirm Destination button text")
//        button.setTitle(R.string.LOGIN_BUT[LANG_KEY], for: .normal)
        self.view.updateFocusIfNeeded()
    }
    
    @objc func tapLocation(sender:UITapGestureRecognizer) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        
    }
    
    @IBAction func onButtonConfirmLocationClicked() {
        self.disableConfirm()
        if markerMode == .origin {
            map.clear()
            buttonConfirmLocation.titleString = NSLocalizedString("main.button.confirm.destination",value: R.string.PICKUP_2_BUT[LANG_KEY], comment: "Main Page Confirm Destination button text")
            markerMode = .destination
            leftBarButton.setImage(#imageLiteral(resourceName: "back"),for: .normal)
            pickUpLocation = map.camera.target
            Travel.shared.pickupPoint = pickUpLocation
//            imageMarker.image = UIImage(named: "marker_destination")
//            if markerPickup.map == nil {
//                markerPickup.map = map
//                markerPickup.icon = UIImage(named: "marker_pickup")
//            }
//            markerPickup.position = pickUpLocation
            let cameraTarget = CLLocationCoordinate2D(latitude: pickUpLocation.latitude + 0.0005, longitude: pickUpLocation.longitude)
            map.animate(toLocation: cameraTarget)
        }
        else {
            
            LoadingOverlay.shared.showOverlay(view: self.view)
            markerMode = .services
            destinationLocation = map.camera.target
            buttonFavorites.isEnabled = false
            buttonCurrentLocation.isEnabled = false
            locationLabel.isHidden = true
            Travel.shared.destinationPoint = destinationLocation
            imageMarker.isHidden = true
            if markerDropOff.map == nil {
                markerDropOff.map = map
                markerDropOff.icon = UIImage(named: "marker_destination")
            }
            markerDropOff.position = destinationLocation
            map.padding = UIEdgeInsets(top: 100, left: 0, bottom: 250, right: 0)
            var bounds = GMSCoordinateBounds()
            bounds = bounds.includingCoordinate(pickUpLocation)
            bounds = bounds.includingCoordinate(destinationLocation)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
            map.animate(with: update)
            map.settings.setAllGesturesEnabled(false)
            RiderSocketManager.shared.calculateFare(pickupLocation: pickUpLocation, destinationLocation: destinationLocation) { result in
                LoadingOverlay.shared.hideOverlayView()
                let result = result as CalculateFareResultEvent
                if(result.hasError()) {
                    result.errorWhisper(view: self.navigationController!)
                    return
                }
                self.containerServices.isHidden = false
                self.servicesViewController?.reloadData()
            }
            
            let startPoint = "\(pickUpLocation.latitude),\(pickUpLocation.longitude)"
            let endPoint = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
            
            drawMapPath(origin: startPoint, destination: endPoint)
        }
    }
    
    func goBackFromServiceSelection() {
        leftBarButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        map.padding = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        markerPickup.map = nil
        markerDropOff.map = nil
        buttonFavorites.isEnabled = true
        buttonCurrentLocation.isEnabled = true
        locationLabel.isHidden = false
        self.containerServices.isHidden = true
        self.imageMarker.isHidden = false
        markerMode = .origin
        map.settings.setAllGesturesEnabled(true)
        map.animate(toLocation: pickUpLocation)
        imageMarker.image = nil   //UIImage(named: "marker_pickup")
        buttonConfirmLocation.titleString = NSLocalizedString("main.button.confirm.pickup",value: R.string.PICKUP_1_BUT[LANG_KEY], comment: "Main Page Confirm Pickup button text")
    }
    
    func drawSearchBarShadow(){
        searchBarView.layer.shadowColor = UIColor.black.cgColor
        searchBarView.layer.shadowRadius = 8
        searchBarView.layer.shadowOpacity = 0.2
        searchBarView.layer.shouldRasterize = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let position = manager.location else {
            let popup = DialogBuilder.getDialogForMessage(message: "Couldn't get current location. use search to find your current place on map or make sure GPS is enabled on your device.", completion: nil)
            self.present(popup, animated: true, completion: nil)
            locationManager.stopUpdatingLocation()
            return
        }
        map.animate(to: GMSCameraPosition.camera(withTarget: position.coordinate, zoom: 16))
        currentLocation = position.coordinate
        locationManager.stopUpdatingLocation()
    }
    
    func enableConfirm() {
        buttonConfirmLocation.isEnabled = true
        buttonConfirmLocation.bgColor = UIColor(hexString: "FFAC08")
        buttonConfirmLocation.layer.shadowOpacity = 0.7
        
    }
    
    func disableConfirm() {
        buttonConfirmLocation.isEnabled = false
        buttonConfirmLocation.bgColor = UIColor(hexString: "939393")
        buttonConfirmLocation.layer.shadowOpacity = 00
    }
    
    @IBAction func onMenuClicked(_ sender: UIButton) {
        switch markerMode {
        case .origin:
            NotificationCenter.default.post(name: .menuClicked, object: nil)
            break
        case .destination:
            
            leftBarButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
            //imageMarker.image = UIImage(named: "marker_pickup")
            map.animate(toLocation: pickUpLocation)
            
            map.clear()
            markerPickup.map = nil
            markerMode = .origin
            buttonConfirmLocation.titleString = NSLocalizedString("main.button.confirm.pickup",value: R.string.PICKUP_1_BUT[LANG_KEY], comment: "Main Page Confirm Pickup button text")
            break
        case .services:
            goBackFromServiceSelection()
            map.clear()
        }
    }
    
    @objc func onStartTravelRequested(notification:Notification) {
        /*let ratingVC = RequestDialogViewController(nibName: "RequestDialogViewController", bundle: nil)
         selectedServiceId: Int = notification.object as? Int
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true) {
            if ratingVC.dialogResult != nil && ratingVC.dialogResult.hasError() {
                ratingVC.dialogResult.errorWhisper(view: self.navigationController!)
                return
            }
            if ratingVC.dialogResult != nil && ratingVC.dialogResult.response == .OK {
                self.performSegue(withIdentifier: "startTravel", sender: nil)
            }
        }
        
        present(popup, animated: true)*/
        
        let selectedServiceId: Int = (notification.object as? Int)!
        RiderSocketManager.shared.requestTaxi(originLocation: (Travel.shared.pickupPoint)!, destinationLocation: (Travel.shared.destinationPoint)!, originAddress: (Travel.shared.pickupAddress)!, destinationAddress: (Travel.shared.destinationAddress)!, serviceId: selectedServiceId) { result in
            let result = result as RequestTaxiResultEvent
            if result.hasError() {
                result.errorWhisper(view: self.navigationController!)
                return
            }
            self.performSegue(withIdentifier: "startLooking", sender: nil)
            self.goBackFromServiceSelection()
        }
        
    }
    
    func drawMapPath(origin: String, destination: String) {

        //Setting the start and end location
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")!
        let dic = NSDictionary(contentsOfFile: path) as! [String: Any]
         GMSPlacesClient.provideAPIKey(dic["GoogleMapsAPIKey"] as! String)
        
        let urls = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(dic["GoogleMapsAPIKey"] as! String)"
        
        let url = URL(string: urls)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in

            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    print(json)
                    let routes = json["routes"] as! NSArray
                    

                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeColor = .black
                            polyline.strokeWidth = 3

                            let bounds = GMSCoordinateBounds(path: path!)
                            self.map!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))

                            polyline.map = self.map

                        }
                    })
                }catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()
    }
    
    @IBAction func onFavoritesClicked(_ sender: UIButton) {
        RiderSocketManager.shared.crudAddress(crud: CRUD.READ, address: Address()) { response,addresses in
            Address.lastDownloaded = addresses
            let style = RMActionControllerStyle.white
            let selectAction = RMAction<UIPickerView>(title: "Select", style: .done) { controller in
                self.map.animate(toLocation: addresses[controller.contentView.selectedRow(inComponent: 0)].location!)
            }
            
            let cancelAction = RMAction<UIPickerView>(title: "Cancel", style: .cancel) { _ in
                print("Row selection was canceled")
            }
            
            let actionController = RMPickerViewController(style: style, title: NSLocalizedString("favorites.picker.title",value: "Favorite Addresses", comment: "Favorites Picker Title"), message: NSLocalizedString("favorites.picker.description",value: "Choose the location from picker in below then use Select button.", comment: "Favorites Picker Description"), select: selectAction, andCancel: cancelAction)!;
            
            actionController.picker.delegate = self;
            actionController.picker.dataSource = self;
            
            if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
                actionController.modalPresentationStyle = UIModalPresentationStyle.popover
            }
            self.present(actionController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func onCurrentLocationClicked(_ sender: UIButton) {
        map.animate(to: GMSCameraPosition.camera(withTarget: currentLocation, zoom: 18))
    }
    
    func getAddressForLatLng(location: CLLocationCoordinate2D) -> String {
        let baseUrl = "https://maps.googleapis.com/maps/api/geocode/json?"
        let apikey = AppDelegate.info["GoogleMapsAPIKey"] as! String
        let url = NSURL(string: "\(baseUrl)latlng=\(location.latitude),\(location.longitude)&key=\(apikey)")
        let data = NSData(contentsOf: url! as URL)
        let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
        if let dictionary = json as? [String: Any] {
            if let results = dictionary["results"] as? [Any] {
                if let firstObject = results.first as? [String:Any] {
                    if let formattedAddress = firstObject["formatted_address"] as? String {
                        return formattedAddress
                    }
                }
            }
        }
        return "Unknown Address"
    }
    
    func getLocationAsync(location: CLLocationCoordinate2D) {
        DispatchQueue.global(qos: .background).async {
            let res = self.getAddressForLatLng(location: location)
            
            DispatchQueue.main.async {
                if(self.map.camera.target.xy == location.xy) {
                    self.locationLabel.text = res
                    self.enableConfirm()
                    if self.markerMode == .origin {
                        Travel.shared.pickupAddress = res
                    } else {
                        Travel.shared.destinationAddress = res
                    }
                }
            }
        }
    }
}

extension MainViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: {
            self.map.animate(toLocation: place.coordinate)
        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
extension MainViewController:GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        disableConfirm()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        getLocationAsync(location: position.target)
        RiderSocketManager.shared.getDriversLocation(point: mapView.camera.target) { serverResponse,result  in
            for driverMarker in self.arrayDriversMarkers {
                driverMarker.map = nil
            }
            self.arrayDriversMarkers.removeAll()
            for location in result {
                let marker = GMSMarker(position: location)
                
                marker.icon = UIGraphicsImageRenderer(size: .init(width: 26.0, height: 57.0)).image { context in
                    UIImage(named: "marker taxi")!.draw(in: .init(origin: .zero, size: context.format.bounds.size))
                }
                
                marker.map = self.map
                self.arrayDriversMarkers.append(marker)
            }
        }
        /*if markerMode == .origin{
            pickUpLocation = position.target
            Travel.shared.pickupPoint = pickUpLocation
        } else {
            destinationLocation = position.target
            Travel.shared.destinationPoint = destinationLocation
        }*/
    }
}

extension MainViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    
    // MARK: UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Address.lastDownloaded.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Address.lastDownloaded[row].title;
    }
}

extension MainViewController: LookingDelegate {
    func cancel() {
        
    }
    
    func found() {
        self.performSegue(withIdentifier: "startTravel", sender: nil)
    }
}
