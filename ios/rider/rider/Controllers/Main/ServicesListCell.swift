//
//  ServicesListCell.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import Kingfisher

class ServicesListCell: UICollectionViewCell {
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textCost: UILabel!
    
    func update(service:Service) {
        titleLabel.text = service.title
        if let media = service.media, let address = media.address {
            let url = URL(string: AppDelegate.info["ServerAddress"] as! String + address)
            imageIcon.kf.setImage(with: url)
        }
//        textCost.text = "Cost: " + String(format:"%.0f ",service.cost!) + currencyUnit[CURRENCY_KEY]
        
        textCost.text = "Cost: " +  formattedNumber(Float(service.cost!).rounded()) + currencyUnit[CURRENCY_KEY]
    }

}
