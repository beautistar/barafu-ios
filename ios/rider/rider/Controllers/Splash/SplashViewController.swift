//
//  SplashViewController.swift
//  Rider
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import ObjectMapper
import Firebase
import FirebaseUI
import OneSignal
import PopupDialog
//import <#module#>

let currencyUnit = ["TZS", "$"]
var LANG_KEY = 0
var CURRENCY_KEY = 0

class SplashViewController: UIViewController {
    let defaults:UserDefaults = UserDefaults.standard
    @IBOutlet weak var indicatorLoading: UIActivityIndicatorView!
    @IBOutlet weak var textLoading: UILabel!
    @IBOutlet weak var buttonLogin: UIButton!
    
    @IBOutlet weak var ui_viewOptions : UIView!
    @IBOutlet weak var ui_txfLng: DropDown!
    @IBOutlet weak var ui_txfCurrency: DropDown!
    
    let arrLng = ["English", "Spanish"]
    let arrCurrency = ["TZS"]  //, "USD"
    
    var dialoagPhoneVC : DialoagPhoneVC!
    var phoneNumSubView: String? = nil
    var countryCode: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LANG_KEY = defaults.integer(forKey: "language")
        CURRENCY_KEY = defaults.integer(forKey: "currency")
        setDropDown()
        
//        if let support =  NSClassFromString("FTDeviceSupport")?.value(forKey: "sharedInstance") as? NSObject {
//           print("phone_num", support.value(forKey: "telephoneNumber")!)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUILanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UIApplication.shared.isRegisteredForRemoteNotifications {
            // equivalent to .authorized case
        }
        else if !UserDefaults.standard.bool(forKey: "didAskForPushPermission")
        {
            let title = NSLocalizedString("message.title.default",value: "Message", comment: "Message Default Title")
            let popup = PopupDialog(title: title, message: NSLocalizedString("question.permission.notification", value: "You can enable notifications so we can let you know of promotions and gift code offerings.", comment: ""))
            
            // Create buttons
            let buttonOne = DefaultButton(title: NSLocalizedString("message.button.ok",value: "OK", comment: "Message OK button")) {
                OneSignal.promptForPushNotifications(userResponse: { accepted in
                })
            }
            let buttonTwo = DefaultButton(title: NSLocalizedString("message.button.cancel",value: "Cancel", comment: "Message Cancel Button")) {
                
            }
            
            popup.addButtons([buttonOne, buttonTwo])
            popup.show(self, sender: nil)
            self.present(popup, animated: true, completion: nil)
            UserDefaults.standard.set(true, forKey: "didAskForPushPermission")
        }
        else {
            // equivalent to .denied case
        }
        
        if let data = UserDefaults.standard.data(forKey: "settings"),
            let settings = NSKeyedUnarchiver.unarchiveObject(with: data) as? AppConfig {
            connectSocket(token: settings.token!)
            AppConfig.shared = settings
        } else {
            indicatorLoading.isHidden = true
            textLoading.isHidden = true
            buttonLogin.isHidden = false
            ui_viewOptions.isHidden = false
        }
    }
    
    func setDropDown() {
        
        ui_txfLng.optionArray = arrLng
        ui_txfLng.optionIds = [0, 1]
        ui_txfLng.isSearchEnable = false
        ui_txfLng.checkMarkEnabled = false
        ui_txfLng.selectedIndex = LANG_KEY
        ui_txfLng.text = arrLng[LANG_KEY]
        ui_txfLng.didSelect{(selectedText , index , id) in
            LANG_KEY = id
            self.defaults.set(index, forKey:"language")
            self.updateUILanguage()
        }
        
        ui_txfCurrency.optionArray = arrCurrency
        ui_txfCurrency.optionIds = [0]
        ui_txfCurrency.isSearchEnable = false
        ui_txfCurrency.checkMarkEnabled = false
        ui_txfCurrency.selectedIndex = CURRENCY_KEY
        ui_txfCurrency.text = arrCurrency[CURRENCY_KEY]
        ui_txfCurrency.didSelect{(selectedText , index , id) in
            CURRENCY_KEY = id
            self.defaults.set(index, forKey:"currency")
        }
    }
    
    func updateUILanguage() {
        textLoading.text = R.string.LOADING_MARK[LANG_KEY]
        buttonLogin.setTitle(R.string.LOGIN_BUT[LANG_KEY], for: .normal)
        self.view.updateFocusIfNeeded()
    }
    
    func connectSocket(token:String) {
        NotificationCenter.default.addObserver(self, selector: #selector(SplashViewController.onSocketError), name: .socketError, object: nil)
        RiderSocketManager.shared.connect(token: token, completionHandler: {
            if let oneSignalAppId = AppDelegate.info["OneSignalAppId"] as? String, !oneSignalAppId.isEmpty {
                let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
                OneSignal.initWithLaunchOptions((UIApplication.shared.delegate as! AppDelegate).launchOptions,
                                                appId: oneSignalAppId,
                                                handleNotificationAction: nil,
                                                settings: onesignalInitSettings)
                
                OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
            }
            self.performSegue(withIdentifier: "segueShowHost", sender: nil)
        })
    }
    
    @IBAction func onLoginClicked(_ sender: UIButton) {
        if (AppDelegate.info["TestModeEnabled"] as! Bool) {
            tryLogin(phoneNumber: (AppDelegate.info["TestModeNumber"] as! String))
        }
        else {
            
            dialoagPhoneVC = (self.storyboard?.instantiateViewController(withIdentifier: "DialoagPhoneVC") as! DialoagPhoneVC)
            dialoagPhoneVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
            dialoagPhoneVC.parentVC = self
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.addSubview(self.dialoagPhoneVC.view)
            })
        }
    }
    
    func closeSubVC(){
        
        UIView.animate(withDuration: 0.3, animations: {
            self.dialoagPhoneVC.view.removeFromSuperview()
        })
        
        if phoneNumSubView == "718238477" && countryCode == "+255" {
            tryLogin(phoneNumber: countryCode! + phoneNumSubView!)
//            tryLogin(phoneNumber: phoneNumSubView!)
            
        } else {
            
            let auth = FUIAuth.defaultAuthUI()
            auth?.delegate = self
            let phoneAuth = FUIPhoneAuth(authUI: auth!)
            auth?.providers = [phoneAuth]
            
            phoneAuth.signIn(withPresenting: self, phoneNumber: countryCode! + phoneNumSubView!)
        }
    }
    
    
    @objc func onSocketError(_ notification: Notification) {
//        let obj = notification.object as! [Any]
//        let dialog = DialogBuilder.getDialogForMessage(message: (obj[0] as? String)!, completion: nil)
//        present(dialog, animated: true, completion: nil)
        indicatorLoading.isHidden = true
        textLoading.isHidden = true
        buttonLogin.isHidden = false
        ui_viewOptions.isHidden = false
    }
    
    func tryLogin(phoneNumber:String) {
        print("phoneNumber: ", phoneNumber)
        
        var request = URLRequest(url: URL(string: (AppDelegate.info["ServerAddress"] as! String) + "rider_login/")!)
        request.httpMethod = "POST"
        let postString = "user_name=" + phoneNumber
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                return
            }
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
            let token = json!["token"] as! String
            AppConfig.shared.token = token
            AppConfig.shared.user = Rider(JSON: json!["user"] as! [String:Any])
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: AppConfig.shared)
            self.defaults.set(encodedData, forKey:"settings")

            self.defaults.set(LANG_KEY, forKey:"language")
            self.defaults.set(CURRENCY_KEY, forKey:"currency")
            self.connectSocket(token: token)
        }
        task.resume()
    }
}
extension SplashViewController: FUIAuthDelegate {
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
    
        print("authUI.provider(withID:phone): ", authUI.provider(withID: "phone"))
        
        if(user == nil){
            return
        }
        
        tryLogin(phoneNumber: (user?.phoneNumber)!)
    }
}
