//
//  DriverInfo.swift
//
//  Copyright (c) Minimalistic Apps. All rights reserved.
//

import Foundation
import ObjectMapper

public final class DriverInfo: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let cost = "cost"
    static let driver = "driver"
    static let duration = "duration"
    static let distance = "distance"
  }

  // MARK: Properties
  public var cost: Double?
  public var driver: Driver?
  public var duration: Int?
  public var distance: Int?

    init() {
        
    }
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    cost <- map[SerializationKeys.cost]
    driver <- map[SerializationKeys.driver]
    duration <- map[SerializationKeys.duration]
    distance <- map[SerializationKeys.distance]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = cost { dictionary[SerializationKeys.cost] = value }
    if let value = driver { dictionary[SerializationKeys.driver] = value }
    if let value = duration { dictionary[SerializationKeys.duration] = value }
    if let value = distance { dictionary[SerializationKeys.distance] = value }
    return dictionary
  }

}
