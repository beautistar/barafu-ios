//
//  Service.swift
//
//  Copyright (c) Minimalistic apps. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Service: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let availableTimeTo = "available_time_to"
    static let perMinutePassed = "per_minute_passed"
    static let availableTimeFrom = "available_time_from"
    static let id = "id"
    static let title = "title"
    static let perHundredMeters = "per_hundred_meters"
    static let media = "media"
    static let baseFare = "base_fare"
    static let serviceCategory = "service_category"
    static let perMinuteWait = "per_minute_wait"
    static let cost = "cost"
  }

  // MARK: Properties
  public var availableTimeTo: String?
  public var perMinutePassed: Float?
  public var availableTimeFrom: String?
  public var id: Int?
  public var title: String?
  public var perHundredMeters: Float?
  public var media: Media?
  public var baseFare: Float?
  public var serviceCategory: ServiceCategory?
  public var perMinuteWait: Float?
  public var cost: Double?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    availableTimeTo <- map[SerializationKeys.availableTimeTo]
    perMinutePassed <- map[SerializationKeys.perMinutePassed]
    availableTimeFrom <- map[SerializationKeys.availableTimeFrom]
    id <- map[SerializationKeys.id]
    title <- map[SerializationKeys.title]
    perHundredMeters <- map[SerializationKeys.perHundredMeters]
    media <- map[SerializationKeys.media]
    baseFare <- map[SerializationKeys.baseFare]
    serviceCategory <- map[SerializationKeys.serviceCategory]
    perMinuteWait <- map[SerializationKeys.perMinuteWait]
    cost <- map[SerializationKeys.cost]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = availableTimeTo { dictionary[SerializationKeys.availableTimeTo] = value }
    if let value = perMinutePassed { dictionary[SerializationKeys.perMinutePassed] = value }
    if let value = availableTimeFrom { dictionary[SerializationKeys.availableTimeFrom] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = perHundredMeters { dictionary[SerializationKeys.perHundredMeters] = value }
    if let value = media { dictionary[SerializationKeys.media] = value }
    if let value = baseFare { dictionary[SerializationKeys.baseFare] = value }
    if let value = serviceCategory { dictionary[SerializationKeys.serviceCategory] = value }
    if let value = perMinuteWait { dictionary[SerializationKeys.perMinuteWait] = value }
    if let value = cost { dictionary[SerializationKeys.cost] = value }
    return dictionary
  }

}
