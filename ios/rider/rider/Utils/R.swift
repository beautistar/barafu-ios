//
//  File.swift
//  driver
//
//  Created by RMS on 10/29/19.
//  Copyright © 2019 minimal. All rights reserved.
//
import Foundation

struct R {
    
    struct string {
        
        static let APP_TITLE = "Barafu Ride"
        
        //splash screen
        static let LOADING_MARK = ["Loading...", "Cargando..."]
        static let LOGIN_BUT = ["LOGIN", "INICIAR SESIÓN"]
        
        //main page
        static let PICKUP_1_BUT = ["CONFIRM PICKUP", "CONFIRMAR RECOGIDA"]
        static let PICKUP_2_BUT = ["CONFIRM DESTINATION", "CONFIRMAR DESTINO"]
        
        
        //menu page
        static let BUT_TRIPHISTORY = ["Trip History", "Historia de viaje"]
        static let BUT_YOURPROFILE = ["Your Profile", "Tu perfil"]
        static let BUT_LOCATION = ["Location", "Localizaciones"]
        static let BUT_WALLET = ["Wallet", "Billetera"]
        static let BUT_COUPONS = ["Coupons", "Cupones"]
        static let BUT_PORMOTIONS = ["Promotions", "Promociones"]
        static let BUT_TRANSACTIONS = ["Transactions", "Actas"]
        static let BUT_SUPPORT = ["Support", "Apoyo"]
        static let BUT_EMERGENCY = ["Emergency", "Emergencia"]
        static let BUT_ABOUT = ["About", "Acerca de"]
        static let BUT_LOGOUT = ["Logout", "Cerrar sesión"]
        
        static let T_BALANCE = ["Balance", "Equilibrar"]
        
        //service child
        static let B_CONFIRM = ["CONFIRM SERVICE", "CONFIRMAR SERVICIO"]
        
        
        static let BUT_TRIPHISTORY12313 = ["Logout", "Historia"]
        static let ABOUT2 = ["About", "Acerca"]
        
        
        
        
        
        
        
        //support / about
        
        static let ABOUT_TITLE = ["About", "Acerca de"]
        static let ABOUT_TOP = ["Be safe, be on time", "Estar a salvo, llegar a tiempo"]
        static let WE_ARE_AN_INNOVATION = ["We are an innovative Tanzanian transportation platform that enables people to gain short-term access to transportation modes on an as-needed basis, at an affordable competitive price.", "Somos una innovadora plataforma de transporte de Tanzania que permite a las personas obtener acceso a corto plazo a los modos de transporte según sea necesario, a un precio competitivo asequible."]
        static let THIS_IS_OUR_MOTTO = ["This is our motto at Barafu ride.","Este es nuestro lema en Barafu ride."]
        static let SAFELY_TIME_AND_EXCELLENT = ["Safety, Time and Excellent services is our focus.", "La seguridad, el tiempo y los excelentes servicios son nuestro enfoque."]
        static let WE_CALL_FOR_EVERY_RIDER = ["We call for every rider and driver to agree to our Community Guiding principles before they can take or give a ride with us. We believe every ride should be a welcoming and safe space for everybody."]
        static let WE_HAVE_A_DEDICATED = ["We have a dedicated Trust and Safety team that’s available to speak with you and assist you through our around-the-clock Critical Response Line."]
        static let CONTACT_US_FOR = ["Contact us for assistance", "Contáctenos para asistencia"]
        
        
        
        
        //Emergency
        static let EMERGENCY_TITLE = ["Emergency Contact", "Contacto de emergencia"]
        
        static let EMERGENCY_TOP = ["Make your travel safe", "Haz tu viaje seguro"]
        static let EMERGENCY_TEXT1 = ["Send alert to your friends/family members in case of emergency", "Enviar alertas a sus amigos / familiares en caso de emergencia"]
        static let EMERGENCY_TEXT2 = ["Please add them to your emergency contacts", "Agréguelos a sus contactos de emergencia."]
        static let EMERGENCY_ADD = ["ADD CONTACTS", "AÑADIR CONTACTOS"]
        
        
        //about
        static let ABOUT = ["About", "Acerca de"]
        static let INFO = ["Application Name", "Nombre de la aplicación"]
        
        static let VERSION = ["Version", "Versión"]
        static let WEBSITE = ["Website", "Sitio web"]
        static let PHONENUM = ["Phone Number", "Número de teléfono"]
        static let APPS_ALL = ["Apps All rights reserved","Apps Todos los derechos reservados"]
        
        
        
        static let OK = ["OK", "Okay"]
        static let CANCEL = ["CANCEL", "CANCELAR"]
        static let ERROR = ["Error!", "Error"]
        static let SUCCESS = ["Success!", "Éxito"]
        static let CONNECT_FAIL = ["No internet","No internet"]
        static let BACK = ["Back","atrás"]
        
    }
    
   
}
