//
//  UserDefault.swift
//
//  Created by AAA on 13/06/2017.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

private let FADefaults = UserDefaults.standard

class UserDefault: NSObject {

    class func getObject(key: String) -> AnyObject? {
        return FADefaults.object(forKey: key) as AnyObject?
    }
    
    class func getInt(key: String) -> Int {
        return FADefaults.integer(forKey: key)
    }
    
    class func getBool(key: String) -> Bool {
        return FADefaults.bool(forKey: key)
    }
    
    class func getFloat(key: String) -> Float {
        return FADefaults.float(forKey: key)
    }
    
    class func getString(key: String) -> String? {
        return FADefaults.string(forKey: key)
    }
    
    class func getData(key: String) -> NSData? {
        return FADefaults.data(forKey: key) as NSData?
    }
    
    class func getArray(key: String) -> NSArray? {
        return FADefaults.array(forKey: key) as NSArray?
    }
    
    class func getDictionary(key: String) -> NSDictionary? {
        return FADefaults.dictionary(forKey: key) as NSDictionary?
    }
    
    // MARK: - getter with default value
    class func getObject(key: String, defaultValue: AnyObject) -> AnyObject? {
        if getObject(key: key) == nil {
            return defaultValue 
        }
        return getObject(key: key)
    }
    
    class func getInt(key: String, defaultValue: Int) -> Int {
        if getObject(key: key) == nil {
            return defaultValue
        }
        return getInt(key: key)
    }
    
    class func getBool(key: String, defaultValue: Bool) -> Bool {
        if getObject(key: key) == nil {
            return defaultValue
        }
        return getBool(key: key)
    }
    
    class func getFloat(key: String, defaultValue: Float) -> Float {
        if getObject(key: key) == nil {
            return defaultValue
        }
        return getFloat(key: key)
    }
    
    class func getString(key: String, defaultValue: String) -> String? {
        if getObject(key: key) == nil {
            return defaultValue
        }
        return getString(key: key)
    }
    
    class func getData(key: String, defaultValue: NSData) -> NSData? {
        if getObject(key: key) == nil {
            return defaultValue
        }
        return getData(key: key)
    }
    
    class func getArray(key: String, defaultValue: NSArray) -> NSArray? {
        if getObject(key: key) == nil {
            return defaultValue
        }
        return getArray(key: key)
    }
    
    class func getDictionary(key: String, defaultValue: NSDictionary) -> NSDictionary? {
        if getObject(key: key) == nil {
            return defaultValue
        }
        return getDictionary(key: key)
    }
    
    // MARK: - Setter
    class func setObject(key: String, value: AnyObject?) {
        if value == nil {
            FADefaults.removeObject(forKey: key)
        } else {
            FADefaults.set(value, forKey: key)
        }
        FADefaults.synchronize()
    }
    
    class func setInt(key: String, value: Int) {
        FADefaults.set(value, forKey: key)
        FADefaults.synchronize()
    }
    
    class func setBool(key: String, value: Bool) {
        FADefaults.set(value, forKey: key)
        FADefaults.synchronize()
    }
    
    class func setFloat(key: String, value: Float) {
        FADefaults.set(value, forKey: key)
        FADefaults.synchronize()
    }
    
    class func setString(key: String, value: NSString?) {
        if (value == nil) {
            FADefaults.removeObject(forKey: key)
        } else {
            FADefaults.set(value, forKey: key)
        }
        FADefaults.synchronize()
    }
    
    class func setData(key: String, value: NSData) {
        setObject(key: key, value: value)
    }
    
    class func setArray(key: String, value: NSArray) {
        setObject(key: key, value: value)
    }
    
    class func setDictionary(key: String, value: NSDictionary) {
        setObject(key: key, value: value)
    }
    
    // MARK: - Synchronize
    class func Sync() {
        FADefaults.synchronize()
    }
    
    
    
    
}
