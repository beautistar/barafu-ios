//
//  MyPickerView.swift
//  rider
//
//  Created by RMS on 10/29/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import Foundation
import UIKit

class MyPickerView : UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    
    // Sets number of columns in picker view
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Sets the number of rows in the picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
 
    var pickerData : [String]!
    var pickerTextField : UITextField!
    var selectionHandler : ((String, Int) -> Void)?
 
    init(pickerData: [String], dropdownField: UITextField) {
        super.init(frame: .zero)
 
        self.pickerData = pickerData
        self.pickerTextField = dropdownField
 
        self.delegate = self
        self.dataSource = self
 
        DispatchQueue.main.async {
            if pickerData.count > 0 {
                self.pickerTextField.text = self.pickerData[0]
                self.pickerTextField.isEnabled = true
            } else {
                self.pickerTextField.text = nil
                self.pickerTextField.isEnabled = false
            }
        }
        
        if self.pickerTextField.text != nil  && self.selectionHandler != nil {

            var ind = 0
            for index in 0 ..< pickerData.count {
                if pickerData[index] == self.pickerTextField.text! {
                    ind = index
                    return
                }
            }
            
            selectionHandler!(self.pickerTextField.text!, ind)
        }
    }
 
    convenience init(pickerData: [String], dropdownField: UITextField, onSelect selectionHandler: @escaping (_ selectedText: String, _ selectedIndex : Int) -> Void) {
 
        self.init(pickerData: pickerData, dropdownField: dropdownField)
        
        self.selectionHandler = selectionHandler
    }
 
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    // This function sets the text of the picker view to the content of the "salutations" array
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
 
    // When user selects an option, this function will set the text of the text field to reflect
    // the selected option.
    private func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerTextField.text = pickerData[row]
 
        if self.pickerTextField.text != nil && self.selectionHandler != nil {
            selectionHandler!(self.pickerTextField.text!, row)
        }
    }
}
