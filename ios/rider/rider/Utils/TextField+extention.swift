//
//  TextField+extention.swift
//  rider
//
//  Created by RMS on 10/29/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import Foundation
import UIKit
 
extension UITextField {
    func loadDropdownData(data: [String]) {
        self.inputView = MyPickerView(pickerData: data, dropdownField: self)
    }
 
    func loadDropdownData(data: [String], onSelect selectionHandler : @escaping (_ selectedText: String, _ selectedIndex: Int)->Void) {
        self.inputView = MyPickerView(pickerData: data, dropdownField: self, onSelect: selectionHandler)
    }
}
