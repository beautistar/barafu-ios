//
//  BaseResultEvent.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation
import UIKit
//import Whisper
import StatusAlert

class BaseResultEvent{
    var response:ServerResponse
    var message:String?
    
    init(code: Int){
        self.response = ServerResponse(rawValue: code)!
        //TODO: Cancel public timer maybe?
    }
    
    convenience init(code: Int, message:String) {
        self.init(code: code)
        self.message = message
    }
    
    func hasError() -> Bool{
        return response.rawValue != 200
    }
    
    private func getErrorMessage()->String{
        if message != nil{
            return message!
        }
        return "Error" + String(response.rawValue)
    }
    
    func showErrorDialog(){
        
    }
    func errorWhisper(view:UINavigationController){
        //let message = Message(title: self.response.name, backgroundColor: .red)
        
        // Show and hide a message after delay
        /*Whisper.show(whisper: message, to: view, action: .show)*/
        // Creating StatusAlert instance
        let statusAlert = StatusAlert()
        //statusAlert.title = "Message"
        statusAlert.message = self.response.errorMessage
        statusAlert.canBePickedOrDismissed = true
        
        // Presenting created instance
        statusAlert.showInKeyWindow()
    }
}
