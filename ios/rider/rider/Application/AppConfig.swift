//
//  AppConfig.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import Foundation

class AppConfig: NSObject, NSCoding {
    static var shared = AppConfig()
    var token: String?
    var user: Rider?
    
    init(token: String?, user: Rider?) {
        self.token = token
        self.user = user
        super.init()
    }
    override init() {
        super.init()
    }
    
    // MARK: NSCoding
    required convenience init?(coder decoder: NSCoder) {
        guard let token = decoder.decodeObject(forKey: "token") as? String,
            let user = Rider(JSONString:(decoder.decodeObject(forKey: "user") as? String)!)
            else { return nil }

        self.init(token: token, user: user)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.token, forKey: "token")
        coder.encode(self.user?.toJSONString(), forKey: "user")
    }
}
