//
//  ApiManager.swift
//  rider
//
//  Created by RMS on 10/30/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiManager: NSObject {
    
    class func getDriverInfoGet(driverId: Int, completion: @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let URL = "http://3.130.44.140/app/api/getdriverInfo/\(driverId)"
        Alamofire.request(URL, method:.get).responseJSON { response in
            switch response.result {
                case .failure: completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let result_code = dict["result_code"].intValue
                    if result_code == 200 {
                        completion(true, dict["driver_info"])
                    } else {
                        completion(false, result_code)
                    }
            }
        }
    }
    
}
