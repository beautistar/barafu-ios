//
//  WalletCardVC.swift
//  rider
//
//  Created by RMS on 10/31/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit

class WalletCardVC: UIViewController {

    @IBOutlet weak var ui_balance: UILabel!
    @IBOutlet weak var ui_curBalance: UILabel!
    @IBOutlet weak var ui_selectPay: UILabel!
    
    @IBOutlet weak var ui_amount: UILabel!
    @IBOutlet weak var ui_checkout: UIButton!
    
    @IBOutlet weak var ui_unit1: UIButton!
    @IBOutlet weak var ui_unit2: UIButton!
    @IBOutlet weak var ui_unit3: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let balance = AppConfig.shared.user?.balance {
        
            ui_balance.text = currencyUnit[CURRENCY_KEY] + " " + formattedNumber(Float(balance).rounded())
        }
    }
    
    @IBAction func onTapedMpesa(_ sender: Any) {
        
        guard let url = URL(string: "itms://apps.apple.com/us/app/tigo-pesa-tanzania/id825924072") else { return }
        if UIApplication.shared.canOpenURL(url) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func onTapedTigo(_ sender: Any) {
    
        guard let url = URL(string: "itms://apps.apple.com/us/app/m-pesa-tanzania/id1313948420") else { return }
        if UIApplication.shared.canOpenURL(url) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func onTapedAretel(_ sender: Any) {
    
        guard let url = URL(string: "itms://apps.apple.com/us/app/my-airtel-africa/id1462268018") else { return }
        if UIApplication.shared.canOpenURL(url) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    @IBAction func onTapedCheckout(_ sender: Any) {
    }
    
}
