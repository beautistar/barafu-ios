//
//  ChargeWalletViewController.swift
//  Rider
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import UIKit
import Eureka
import Stripe
import Braintree
import BraintreeDropIn

class WalletViewController: FormViewController {
    var paymentField: STPPaymentCardTextField?
    var amount: Double?
    var paymentMethod: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section(NSLocalizedString("wallet.section.method",value: "Payment Method", comment: "Wallet's Payment method section header"))
            <<< SegmentedRow<String>() {
                $0.options = ["Stripe", "Braintree"]
                $0.value = "stripe"
                }.onChange { row in
                    self.paymentMethod = row.value!.lowercased()
                    if let section = self.form.sectionBy(tag: "card")
                    {
                        if(self.paymentMethod == "braintree") {
                            section.hidden = true
                        } else {
                            section.hidden = false
                        }
                        section.evaluateHidden()
                    }
            }
            +++ Section(NSLocalizedString("wallet.section.card", value: "Card Info", comment: "Walle's Card Info section header")) { section in
                section.tag = "card"
                section.hidden = true
            }
            <<< StripeRow() { row in
                row.tag = "cardNumber"
                }
                .cellUpdate { cell, row in
                    self.paymentField = cell.paymentField
            }
            +++ Section(NSLocalizedString("wallet.section.amount", value: "Amount", comment: "Wallet's amount section header"))
            <<< StepperRow() {
                $0.tag = "amount"
                $0.title = NSLocalizedString("wallet.field.amount.description", value: "Amount of credit you want to charge the wallet.", comment: "Wallet's amount field description")
        }
    }
    
    func startBraintreePayment() {
        // TODO: Switch this URL to your own authenticated API
        let clientTokenURL = NSURL(string: "\(AppDelegate.info["ServerAddress"] as! String)braintree_client_token")!
        let clientTokenRequest = NSMutableURLRequest(url: clientTokenURL as URL)
        clientTokenRequest.setValue("text/plain", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: clientTokenRequest as URLRequest) { (data, response, error) -> Void in
            // TODO: Handle errors
            if data != nil {
                
                let clientToken = String(data: data!, encoding: String.Encoding.utf8)!
                
                self.showDropIn(clientTokenOrTokenizationKey: clientToken)
            }
            
            }.resume()
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                LoadingOverlay.shared.hideOverlayView()
                DialogBuilder.alertOnError(message: (error?.localizedDescription)!)
            } else if (result?.isCancelled == true) {
                LoadingOverlay.shared.hideOverlayView()
                DialogBuilder.alertOnError(message: NSLocalizedString("alert.error.user.canceled", value: "User Canceled", comment: "alert for user canceling payment"))
            } else if let result = result {
                self.doPayment(method: "braintree", token: (result.paymentMethod?.nonce)!, amount: self.amount!)
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
    func startStripePayment() {
        guard let _paymentField = paymentField, let _ = _paymentField.cardNumber else {
            DialogBuilder.alertOnError(message: NSLocalizedString("alert.error.card.missing", value: "Card Info Missing", comment: "alert for card info not being entered"))
            return
        }
        //let cardParams = _paymentField.cardParams
        
        let cardParams = STPCardParams()
        cardParams.number = _paymentField.cardNumber
        cardParams.expMonth = _paymentField.expirationMonth
        cardParams.expYear = _paymentField.expirationYear
        cardParams.cvc = _paymentField.cvc
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                LoadingOverlay.shared.hideOverlayView()
                let dialog = DialogBuilder.getDialogForMessage(message: error.debugDescription,completion: nil)
                self.present(dialog, animated: true, completion: nil)
                return
            }
            self.doPayment(method: "stripe", token: token.tokenId, amount: self.amount!)
        }
    }
    
    @IBAction func onCheckoutClicked(_ sender: Any) {
        let amountRow: StepperRow? = form.rowBy(tag: "amount")
        guard let amountRowValue = amountRow?.value else {
            DialogBuilder.alertOnError(message: NSLocalizedString("alert.error.amount.missing", value: "Amount Missing", comment: "Amount is not entered for payment"))
            return
        }
        self.amount = Double(amountRowValue)
        LoadingOverlay.shared.showOverlay(view: self.navigationController?.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
            LoadingOverlay.shared.hideOverlayView()
        }
        if(self.paymentMethod == "stripe") {
            self.startStripePayment()
        } else {
            self.startBraintreePayment()
        }
        
    }
    
    func doPayment(method: String, token: String, amount: Double) {
        RiderSocketManager.shared.chargeAccount(method: method, token: token, amount: amount) { (response,message) in
            LoadingOverlay.shared.hideOverlayView()
            if message == "" {
                _ = self.navigationController?.popViewController(animated: true)
                DialogBuilder.alertOnSuccess(message: NSLocalizedString("message.payment.succeed", value: "Payment Succeeded", comment: ""))
            } else {
                let dialog =  DialogBuilder.getDialogForMessage(message: response.description,completion: nil)
                self.present(dialog, animated: true,completion: nil)
            }
        }
    }
}
