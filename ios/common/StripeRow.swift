import UIKit
import Eureka
import Stripe

final class StripeCell: Cell<String>, CellType, STPPaymentCardTextFieldDelegate {
    
    var paymentField = STPPaymentCardTextField()
    
    required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        paymentField = STPPaymentCardTextField()
        paymentField.frame = contentView.bounds
        self.contentView.addSubview(paymentField)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setup() {
        super.setup()
        
        selectionStyle = .none
        
        paymentField.backgroundColor = .white
        paymentField.layer.cornerRadius = 0.0
        paymentField.layer.borderWidth = 0.0
        paymentField.delegate = self
        
        height = { return 44 }
    }
    
    override func update() {
        super.update()
        textLabel?.text = nil
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        formViewController()?.beginEditing(of: self)
    }
}

/// An Eureka Row for Stripe's STPPaymentCardTextField.
final class StripeRow: Row<StripeCell>, RowType {
    required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<StripeCell>()
    }
}
