//
//  Coupon.swift
//
//  Created by Minimalistic apps on 11/15/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Coupon: Mappable {

    public static var shared = Coupon()
  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let startAt = "start_at"
    static let isEnabled = "is_enabled"
    static let manyTimesUserCanUse = "many_times_user_can_use"
    static let descriptionValue = "description"
    static let manyUsersCanUse = "many_users_can_use"
    static let isFirstTravelOnly = "is_first_travel_only"
    static let expirationAt = "expiration_at"
    static let maximumCost = "maximum_cost"
    static let id = "id"
    static let code = "code"
    static let creditGift = "credit_gift"
    static let title = "title"
    static let discountFlat = "discount_flat"
    static let minimumCost = "minimum_cost"
    static let discountPercent = "discount_percent"
  }

  // MARK: Properties
  public var startAt: Date?
  public var isEnabled: Int?
  public var manyTimesUserCanUse: Int?
  public var descriptionValue: String?
  public var manyUsersCanUse: Int?
  public var isFirstTravelOnly: Bool?
  public var expirationAt: Date?
  public var maximumCost: Int?
  public var id: Int?
  public var code: String?
  public var creditGift: Int?
  public var title: String?
  public var discountFlat: Int?
  public var minimumCost: Int?
  public var discountPercent: Int?

    init(){
        
    }
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    startAt <- (map[SerializationKeys.startAt],TimestampToDateTransform())
    isEnabled <- map[SerializationKeys.isEnabled]
    manyTimesUserCanUse <- map[SerializationKeys.manyTimesUserCanUse]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    manyUsersCanUse <- map[SerializationKeys.manyUsersCanUse]
    isFirstTravelOnly <- map[SerializationKeys.isFirstTravelOnly]
    expirationAt <- (map[SerializationKeys.expirationAt],TimestampToDateTransform())
    maximumCost <- map[SerializationKeys.maximumCost]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    creditGift <- map[SerializationKeys.creditGift]
    title <- map[SerializationKeys.title]
    discountFlat <- map[SerializationKeys.discountFlat]
    minimumCost <- map[SerializationKeys.minimumCost]
    discountPercent <- map[SerializationKeys.discountPercent]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = startAt { dictionary[SerializationKeys.startAt] = value }
    if let value = isEnabled { dictionary[SerializationKeys.isEnabled] = value }
    if let value = manyTimesUserCanUse { dictionary[SerializationKeys.manyTimesUserCanUse] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = manyUsersCanUse { dictionary[SerializationKeys.manyUsersCanUse] = value }
    if let value = isFirstTravelOnly { dictionary[SerializationKeys.isFirstTravelOnly] = value }
    if let value = expirationAt { dictionary[SerializationKeys.expirationAt] = value }
    if let value = maximumCost { dictionary[SerializationKeys.maximumCost] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = creditGift { dictionary[SerializationKeys.creditGift] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = discountFlat { dictionary[SerializationKeys.discountFlat] = value }
    if let value = minimumCost { dictionary[SerializationKeys.minimumCost] = value }
    if let value = discountPercent { dictionary[SerializationKeys.discountPercent] = value }
    return dictionary
  }

}
