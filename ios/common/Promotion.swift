//
//  Promotion.swift
//
//  Created by Minimalistic Apps on 11/15/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Promotion: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionValue = "description"
    static let title = "title"
    static let id = "id"
    static let startTimestamp = "start_timestamp"
    static let expirationTimestamp = "expiration_timestamp"
  }

  // MARK: Properties
  public var descriptionValue: String?
  public var title: String?
  public var id: Int?
  public var startTimestamp: String?
  public var expirationTimestamp: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    descriptionValue <- map[SerializationKeys.descriptionValue]
    title <- map[SerializationKeys.title]
    id <- map[SerializationKeys.id]
    startTimestamp <- map[SerializationKeys.startTimestamp]
    expirationTimestamp <- map[SerializationKeys.expirationTimestamp]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = startTimestamp { dictionary[SerializationKeys.startTimestamp] = value }
    if let value = expirationTimestamp { dictionary[SerializationKeys.expirationTimestamp] = value }
    return dictionary
  }

}
