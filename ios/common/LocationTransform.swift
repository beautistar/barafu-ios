//
//  LocationTransform.swift
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreLocation

public class LocationTransform: TransformType {
    public typealias Object = CLLocationCoordinate2D
    public typealias JSON = [Double]
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> CLLocationCoordinate2D? {
        if let coordList = value as? [Double], coordList.count == 2 {
            return CLLocationCoordinate2D(latitude: coordList[0], longitude: coordList[1])
        }
        return nil
    }
    
    public func transformToJSON(_ value: CLLocationCoordinate2D?) -> [Double]? {
        if let location = value {
            return [Double(location.latitude), Double(location.longitude)]
        }
        return nil
    }
}
