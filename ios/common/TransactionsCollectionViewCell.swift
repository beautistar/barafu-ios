//
//  TripHistoryCollectionViewCell.swift
//  rider
//
//  Copyright © 2018 minimal. All rights reserved.
//

import UIKit
import LGButton

class TransactionsCollectionViewCell: UICollectionViewCell {
    public var transaction: Transaction?
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var textdescription: UILabel!
    @IBOutlet weak var rootView: LGButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        rootView.gradientStartColor = nil
        rootView.gradientEndColor = nil
        rootView.shadowColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let tr = transaction {
            title.text = tr.transactionType?.capitalizingFirstLetter()
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .medium
            
            if let startTimestamp = tr.transactionTime {
                textdescription.text = "\(String(format: NSLocalizedString("base.money", value: "%.0f " + currencyUnit[CURRENCY_KEY], comment: "Default Currency format. 0 stands for digits after '.'"), tr.amount!)) at \(dateFormatter.string(from: startTimestamp))"
            }
        }
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
