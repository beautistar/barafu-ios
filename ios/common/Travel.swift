//
//  Travel.swift
//
//  Copyright (c) Minimalistic Apps. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

public final class Travel: Mappable {
    public static var shared = Travel()
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let destinationAddress = "destination_address"
        static let pickupAddress = "pickup_address"
        static let durationReal = "duration_real"
        static let distanceReal = "distance_real"
        static let requestTime = "request_time"
        static let startTimstamp = "start_timestamp"
        static let finishTimstamp = "finish_timestamp"
        static let driver = "driver"
        static let coupon = "coupon"
        static let rating = "rating"
        static let costBest = "cost_best"
        static let rider = "rider"
        static let distanceBest = "distance_best"
        static let status = "status"
        static let durationBest = "duration_best"
        static let cost = "cost"
        static let id = "id"
        static let isHidden = "is_hidden"
        static let destinationPoint = "destination_point"
        static let log = "log"
        static let pickupPoint = "pickup_point"
        static let rider_id = "rider_id"
        
    }
    
    // MARK: Properties
    public var destinationAddress: String?
    public var pickupAddress: String?
    public var durationReal: Int?
    public var distanceReal: Int?
    public var requestTime: String?
    public var startTimestamp: Date?
    public var finishTimestamp: Date?
    public var driver: Driver?
    public var coupon: Coupon?
    public var rating: Int?
    public var costBest: Double?
    public var rider: Rider?
    public var distanceBest: Int?
    public var status: String?
    public var durationBest: Int?
    public var cost: Double?
    public var id: Int?
    public var isHidden: Int?
    public var destinationPoint: CLLocationCoordinate2D?
    public var log: String?
    public var pickupPoint: CLLocationCoordinate2D?
    public var rider_id: Int?
    init() {
        
    }
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        destinationAddress <- map[SerializationKeys.destinationAddress]
        pickupAddress <- map[SerializationKeys.pickupAddress]
        durationReal <- map[SerializationKeys.durationReal]
        distanceReal <- map[SerializationKeys.distanceReal]
        requestTime <- map[SerializationKeys.requestTime]
        startTimestamp <- (map[SerializationKeys.startTimstamp],TimestampToDateTransform())
        finishTimestamp <- (map[SerializationKeys.finishTimstamp],TimestampToDateTransform())
        driver <- map[SerializationKeys.driver]
        coupon <- map[SerializationKeys.coupon]
        rating <- map[SerializationKeys.rating]
        costBest <- map[SerializationKeys.costBest]
        rider <- map[SerializationKeys.rider]
        distanceBest <- map[SerializationKeys.distanceBest]
        status <- map[SerializationKeys.status]
        durationBest <- map[SerializationKeys.durationBest]
        cost <- map[SerializationKeys.cost]
        id <- map[SerializationKeys.id]
        isHidden <- map[SerializationKeys.isHidden]
        destinationPoint <- (map[SerializationKeys.destinationPoint],LocationXYTransform())
        log <- map[SerializationKeys.log]
        pickupPoint <- (map[SerializationKeys.pickupPoint],LocationXYTransform())
        rider_id <- map[SerializationKeys.rider_id]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = destinationAddress { dictionary[SerializationKeys.destinationAddress] = value }
        if let value = pickupAddress { dictionary[SerializationKeys.pickupAddress] = value }
        if let value = durationReal { dictionary[SerializationKeys.durationReal] = value }
        if let value = distanceReal { dictionary[SerializationKeys.distanceReal] = value }
        if let value = requestTime { dictionary[SerializationKeys.requestTime] = value }
        if let value = startTimestamp { dictionary[SerializationKeys.startTimstamp] = value }
        if let value = finishTimestamp { dictionary[SerializationKeys.finishTimstamp] = value }
        if let value = driver { dictionary[SerializationKeys.driver] = value }
        if let value = coupon { dictionary[SerializationKeys.coupon] = value }
        if let value = rating { dictionary[SerializationKeys.rating] = value }
        if let value = costBest { dictionary[SerializationKeys.costBest] = value }
        if let value = rider { dictionary[SerializationKeys.rider] = value }
        if let value = distanceBest { dictionary[SerializationKeys.distanceBest] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = durationBest { dictionary[SerializationKeys.durationBest] = value }
        if let value = cost { dictionary[SerializationKeys.cost] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = isHidden { dictionary[SerializationKeys.isHidden] = value }
        if let value = destinationPoint { dictionary[SerializationKeys.destinationPoint] = value }
        if let value = log { dictionary[SerializationKeys.log] = value }
        if let value = pickupPoint { dictionary[SerializationKeys.pickupPoint] = value }
        if let value = rider_id { dictionary[SerializationKeys.rider_id] = value }
        return dictionary
    }
    
}

