//
//  DialogBuilder.swift
//  common
//
//  Copyright © 2018 minimalistic apps. All rights reserved.
//

import PopupDialog
import StatusAlert

public class DialogBuilder{
    public enum ButtonOptions{
        case OK_CANCEL, RETRY_CANCEL, OK
    }
    public enum DialogResult{
        case OK, CANCEL, RETRY
    }
        
    public static func getDialogForResponse(response:[String:Any], result: @escaping (DialogResult)->Void) -> PopupDialog {
        // Prepare the popup assets
        let title = NSLocalizedString("message.title.error", value: "Error Happened", comment: "Default title for any error occured")
        let message:String
        if (response["status"] as! Int) != 666 {
            message = (ServerResponse(rawValue: response["status"] as! Int)?.errorMessage)!
        } else {
            message = response["error"] as! String
        }
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = CancelButton(title: NSLocalizedString("message.button.cancel",value: "Cancel", comment: "Message Cancel Button")) {
            result(.CANCEL)
        }
        
        let buttonTwo = DefaultButton(title: NSLocalizedString("message.button.retry", value: "Retry", comment: "Message Retry Button")) {
            result(.RETRY)
        }
        
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonOne, buttonTwo])
        
        return popup
    }
    
    public static func getDialogForMessage(message:String, completion:((DialogResult)->Void)?) -> PopupDialog {
        // Prepare the popup assets
        let title = NSLocalizedString("message.title.default",value: "Message", comment: "Message Default Title")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Create buttons
        let buttonOne = DefaultButton(title: NSLocalizedString("message.button.ok",value: "OK", comment: "Message OK button")) {
            if let _completion = completion {
                _completion(.OK)
            }
        }
        
        popup.addButtons([buttonOne])
        
        return popup
    }
    
    public static func alertOnResponse(response:ServerResponse) {
        if response == ServerResponse.OK {
            alertOnSuccess(message: NSLocalizedString("alert.info.done", value: "Action Done", comment: "Text of alert action Done"))
        } else {
            alertOnError(message: response.errorMessage)
        }
    }
    
    public static func alertOnError(message:String) {
        // Creating StatusAlert instance
        let statusAlert = StatusAlert()
        statusAlert.title = NSLocalizedString("message.title.error", value: "Error Happened", comment: "Default title for any error occured")
        statusAlert.message = message
        statusAlert.canBePickedOrDismissed = true
        statusAlert.image = UIImage(named: "alert_error")
        // Presenting created instance
        statusAlert.showInKeyWindow()
    }
    
    public static func alertOnSuccess(message:String) {
        // Creating StatusAlert instance
        let statusAlert = StatusAlert()
        //statusAlert.title = "Info"
        statusAlert.image = UIImage(named: "alert_success")
        statusAlert.message = message
        statusAlert.canBePickedOrDismissed = true
        
        // Presenting created instance
        statusAlert.showInKeyWindow()
    }
}
