//
//  Rider.swift
//
//  Copyright (c) Minimalistic Apps. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Rider: Mappable {
    public static var shared : Rider?
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let email = "email"
        static let mobileNumber = "mobile_number"
        static let address = "address"
        static let gender = "gender"
        static let balance = "balance"
        static let lastName = "last_name"
        static let id = "id"
        static let firstName = "first_name"
        static let media = "media"
    }
    
    // MARK: Properties
    public var email: String?
    public var mobileNumber: Int64?
    public var address: String?
    public var gender: String?
    public var balance: Double?
    public var lastName: String?
    public var id: Int?
    public var firstName: String?
    public var media: Media?
    
    init() {
        
    }
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        email <- map[SerializationKeys.email]
        mobileNumber <- map[SerializationKeys.mobileNumber]
        address <- map[SerializationKeys.address]
        gender <- map[SerializationKeys.gender]
        balance <- (map[SerializationKeys.balance],StringToDoubleTransform())
        lastName <- map[SerializationKeys.lastName]
        id <- map[SerializationKeys.id]
        firstName <- map[SerializationKeys.firstName]
        media <- map[SerializationKeys.media]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = mobileNumber { dictionary[SerializationKeys.mobileNumber] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = gender { dictionary[SerializationKeys.gender] = value }
        if let value = balance { dictionary[SerializationKeys.balance] = value }
        if let value = lastName { dictionary[SerializationKeys.lastName] = value }
        if let value = firstName { dictionary[SerializationKeys.firstName] = value }
        return dictionary
    }
    
}

