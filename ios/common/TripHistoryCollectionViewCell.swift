//
//  TripHistoryCollectionViewCell.swift
//  rider
//
//  Copyright © 2018 minimal. All rights reserved.
//

import UIKit
import LGButton

class TripHistoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var finishTimeLabel: UILabel!
    @IBOutlet weak var rootView: LGButton!
    override func prepareForReuse() {
        super.prepareForReuse()
        rootView.gradientStartColor = nil
        rootView.gradientEndColor = nil
        rootView.shadowColor = UIColor.clear
    }
}
