//
//  Transaction.swift
//
//  Created by Minimalistic Apps on 11/15/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Transaction: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let documentNumber = "document_number"
    static let id = "id"
    static let transactionTime = "transaction_time"
    static let amount = "amount"
    static let transactionType = "transaction_type"
  }

  // MARK: Properties
  public var documentNumber: String?
  public var id: Int?
  public var transactionTime: Date?
  public var amount: Double?
  public var transactionType: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    documentNumber <- map[SerializationKeys.documentNumber]
    id <- map[SerializationKeys.id]
    transactionTime <- (map[SerializationKeys.transactionTime],TimestampToDateTransform())
    amount <- map[SerializationKeys.amount]
    transactionType <- map[SerializationKeys.transactionType]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = documentNumber { dictionary[SerializationKeys.documentNumber] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = transactionTime { dictionary[SerializationKeys.transactionTime] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = transactionType { dictionary[SerializationKeys.transactionType] = value }
    return dictionary
  }

}
