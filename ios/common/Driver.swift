//
//  Driver.swift
//
//  Copyright (c) Minimalistic Apps. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Driver: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let email = "email"
        static let car = "car"
        static let mobileNumber = "mobile_number"
        static let carMedia = "car_media"
        static let address = "address"
        static let gender = "gender"
        static let registrationTimestamp = "registration_timestamp"
        static let infoChanged = "info_changed"
        static let reviewCount = "review_count"
        static let carColor = "car_color"
        static let status = "status"
        static let lastName = "last_name"
        static let balance = "balance"
        static let id = "id"
        static let firstName = "first_name"
        static let carPlate = "car_plate"
        static let media = "media"
        static let accountNumber = "account_number"
        static let rating = "rating"
    }
    
    // MARK: Properties
    public var email: String?
    public var car: Car?
    public var mobileNumber: Int64?
    public var carMedia: Media?
    public var address: String?
    public var gender: String?
    public var registrationTimestamp: String?
    public var infoChanged: Int?
    public var reviewCount: Int?
    public var carColor: String?
    public var status: String?
    public var lastName: String?
    public var balance: Double?
    public var id: Int?
    public var firstName: String?
    public var carPlate: String?
    public var media: Media?
    public var accountNumber: String?
    public var rating: Int?
    
    init() {
        
    }
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        email <- map[SerializationKeys.email]
        car <- map[SerializationKeys.car]
        mobileNumber <- map[SerializationKeys.mobileNumber]
        carMedia <- map[SerializationKeys.carMedia]
        address <- map[SerializationKeys.address]
        gender <- map[SerializationKeys.gender]
        registrationTimestamp <- map[SerializationKeys.registrationTimestamp]
        infoChanged <- map[SerializationKeys.infoChanged]
        reviewCount <- map[SerializationKeys.reviewCount]
        carColor <- map[SerializationKeys.carColor]
        status <- map[SerializationKeys.status]
        lastName <- map[SerializationKeys.lastName]
        balance <- (map[SerializationKeys.balance],StringToDoubleTransform())
        id <- map[SerializationKeys.id]
        firstName <- map[SerializationKeys.firstName]
        carPlate <- map[SerializationKeys.carPlate]
        media <- map[SerializationKeys.media]
        accountNumber <- map[SerializationKeys.accountNumber]
        rating <- map[SerializationKeys.rating]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = mobileNumber { dictionary[SerializationKeys.mobileNumber] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = gender { dictionary[SerializationKeys.gender] = value }
        if let value = carColor { dictionary[SerializationKeys.carColor] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = lastName { dictionary[SerializationKeys.lastName] = value }
        if let value = firstName { dictionary[SerializationKeys.firstName] = value }
        if let value = carPlate { dictionary[SerializationKeys.carPlate] = value }
        return dictionary
    }
    
}

