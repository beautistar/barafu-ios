//
//  TimestampToDateTransform.swift
//  rider
//
//  Copyright © 2018 Minimalistic Apps. All rights reserved.
//
import Foundation
import ObjectMapper

public class TimestampToDateTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> Date? {
        if let dateString = value as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let date = dateFormatter.date(from: dateString)
            return date
        }
        return nil
    }
    
    public func transformToJSON(_ value: Date?) -> String? {
        if value != nil {
            return ""
        }
        return nil
    }
}
