//
//  ContactModel.swift
//  rider
//
//  Created by RMS on 10/31/19.
//  Copyright © 2019 minimal. All rights reserved.
//

import UIKit

class ContactModel: NSObject, NSCoding {

    // Person dictionary variable
    var name: String?
//    var numbers: [String]?
    var number : String?
    var avatar : String?
    
    func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name");
        coder.encode(self.number, forKey: "number");
    }
    


//    init(json: NSDictionary) { // Dictionary object
//        self.name = json["name"] as? String
//        self.number = json["number"] as? String
//    }
    
    //, avatar: String
    init(name: String, number: String) { // Dictionary object
        self.name = name
        self.number = number
//        self.avatar = avatar
    }

    required init?(coder aDecoder: NSCoder) {

        self.name = aDecoder.decodeObject(forKey: "name") as? String;
        self.number = aDecoder.decodeObject(forKey: "number") as? String;
//        self.avatar = aDecoder.decodeObject(forKey: "avatar") as? String;
    }
}




