//
//  StringToDoubleTransform.swift
//  driver
//
//  Copyright © 2018 minimal. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

public class StringToDoubleTransform: TransformType {
    public typealias Object = Double
    public typealias JSON = Any
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> Double? {
        if let stringVal = value as? String {
            return Double(string: stringVal)
        }
        return Double(value as! Double)
    }
    
    public func transformToJSON(_ value: Double?) -> Any? {
        return value
    }
}
