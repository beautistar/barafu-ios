//
//  Media.swift
//
//  Copyright (c) Minimalistic apps. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Media: Mappable {

    public enum PathType {
        case absolute
        case relative
    }
    public enum PrivacyLevel {
        case low
        case medium
        case high
    }
  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let address = "address"
    static let privacyLevel = "privacy_level"
    static let title = "title"
    static let type = "type"
    static let pathType = "path_type"
  }

  // MARK: Properties
  public var id: Int?
  public var address: String?
  public var privacyLevel: PrivacyLevel?
  public var title: String?
  public var type: String?
  public var pathType: PathType?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    address <- map[SerializationKeys.address]
    privacyLevel <- map[SerializationKeys.privacyLevel]
    title <- map[SerializationKeys.title]
    type <- map[SerializationKeys.type]
    pathType <- map[SerializationKeys.pathType]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = privacyLevel { dictionary[SerializationKeys.privacyLevel] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = pathType { dictionary[SerializationKeys.pathType] = value }
    return dictionary
  }

}
